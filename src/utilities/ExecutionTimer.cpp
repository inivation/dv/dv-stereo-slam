//
// Created by rokas on 2020-10-07.
//

#include <dv-slam/utilities/ExecutionTimer.h>

ExecutionTimer::ExecutionTimer(int64_t timestampNow, uint32_t frequency) :
    lastExecutionTime(timestampNow) {
    if (frequency > 0) {
        dt = 1000000LL / static_cast<int64_t>(frequency);
    } else {
        dt = 0;
    }
}

bool ExecutionTimer::timeToRun(int64_t timestampNow) {
    if (dt > 0 && timestampNow - lastExecutionTime >= dt) {
        lastExecutionTime = timestampNow;
        return true;
    }
    return false;
}

void ExecutionTimer::reset() {
    lastExecutionTime = 0LL;
}

ExecutionTimer::ExecutionTimer() = default;
