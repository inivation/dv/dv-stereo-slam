//
// Created by rokas on 2021-01-21.
//

#include <dv-slam/utilities/StampedMat.h>

slam::StampedMat::StampedMat(int64_t timestamp, cv::Mat frame) : timestamp(timestamp), frame(std::move(frame)) {}

slam::StampedMat::StampedMat(int64_t timestamp, int64_t startTimestamp, int64_t endTimestamp, cv::Mat frame)
        : timestamp(timestamp), startTimestamp(startTimestamp), endTimestamp(endTimestamp), frame(std::move(frame)) {}
