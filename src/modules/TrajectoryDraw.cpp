//
// Created by rokas on 2020-11-25.
//

#include <functional>

#include <dv-sdk/module.hpp>
#include <opencv2/plot.hpp>

#include <dv-processing/data/pose_base.hpp>

class TrajectoryDraw : public dv::ModuleBase {
private:
    std::vector<double> xCoords;
    std::vector<double> yCoords;
    cv::Size outputSize;
    double minX, minY, maxX, maxY;

public:
    static void initInputs(dv::InputDefinitionList &in) {
        in.addInput("pose", dv::Pose::TableType::identifier);
    }

    static void initTypes(std::vector<dv::Types::Type> &types) {
    	types.push_back(dv::Types::makeTypeDefinition<dv::Pose, dv::Pose>("A point to be plotted"));
    }

    static void initOutputs(dv::OutputDefinitionList &out) {
        out.addFrameOutput("trajectory");
    }

    static const char *initDescription() {
        return "3D trajectory preview in 2D from pose points.";
    }

    TrajectoryDraw() : outputSize(800, 800) {
    	auto poseInput = inputs.getInput<dv::Pose>("pose");
        outputs.getFrameOutput("trajectory").setup(
                outputSize.width, outputSize.height, poseInput.getOriginDescription()
                );

        minX = std::numeric_limits<double>::max();
        maxX = std::numeric_limits<double>::min();
        minY = std::numeric_limits<double>::max();
        maxY = std::numeric_limits<double>::min();
        xCoords.reserve(10000);
        yCoords.reserve(10000);
    }

    static void initConfigOptions(dv::RuntimeConfig &config) {}

    void run() override {
        // Aggregate the pose
        auto pose = inputs.getInput<dv::Pose>("pose").data();
        int64_t timestamp;
		auto x = static_cast<double>(pose->translation.x());
		auto y = static_cast<double>(pose->translation.z());
		timestamp = pose->timestamp;
		xCoords.push_back(x);
		yCoords.push_back(y);
		if (x > maxX) {
			maxX = x;
		}
		if (x < minX) {
			minX = x;
		}
		if (y > maxY) {
			maxY = y;
		}
		if (y < minY) {
			minY = y;
		}
        while (xCoords.size() > 10000) {
            xCoords.erase(xCoords.begin());
        }
        while (yCoords.size() > 10000) {
            yCoords.erase(yCoords.begin());
        }

        // Draw trajectory
        cv::Mat xPlot(1, xCoords.size(), CV_64F, xCoords.data());
        cv::Mat yPlot(1, yCoords.size(), CV_64F, yCoords.data());

#if CV_MAJOR_VERSION == 4
        cv::Ptr<cv::plot::Plot2d> plot = cv::plot::Plot2d::create(xPlot, yPlot);
#else
        cv::Ptr<cv::plot::Plot2d> plot = cv::plot::createPlot2d(xPlot, yPlot);
#endif
        plot->setPlotSize(outputSize.width, outputSize.height);
        plot->setPlotLineWidth(2);
        double plotMinX = std::min(0.0, std::floor(minX));
        double plotMinY = std::min(0.0, std::floor(minY));
        double plotMaxX = std::max(plotMinX + 1.0, std::ceil(maxX));
        double plotMaxY = std::max(plotMinY + 1.0, std::ceil(maxY));

        double plotMin = std::min(plotMinX, plotMinY);
        double plotMax = std::max(plotMaxX, plotMaxY);

        // Keeping the axes sizes same, not to distort the trajectory.
        plot->setMinX(plotMin);
        plot->setMinY(plotMin);
        plot->setMaxX(plotMax);
        plot->setMaxY(plotMax);

        cv::Mat display;
        plot->render(display);

        auto frameout = outputs.getFrameOutput("trajectory");
        frameout << timestamp << display << dv::commit;
    }

};

registerModuleClass(TrajectoryDraw)

