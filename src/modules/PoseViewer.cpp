//
// Created by radam on 2021-02-03.
//

#include "dv-slam/utilities/PoseViewer.h"

#include <functional>

#include <dv-sdk/module.hpp>
#include <dv-processing/core/core.hpp>

#include <dv-processing/data/pose_base.hpp>

#include <Eigen/Core>

#include <atomic>
#include <chrono>
#include <mutex>
#include <thread>

class PoseViewerModule : public dv::ModuleBase {
private:

    const static constexpr int defaultWidth = 640;
    const static constexpr int defaultHeight = 480;
    cv::Size imageSize;


    std::mutex viewerMutex;
    PoseViewer viewer;
    std::thread drawThread;
    std::atomic<bool> shouldTerminate;


public:
    static void initInputs(dv::InputDefinitionList& in) {
    	in.addInput("pose", dv::Pose::TableType::identifier);
    }

    static void initTypes(std::vector<dv::Types::Type>& types) {
    	types.push_back(dv::Types::makeTypeDefinition<dv::Pose, dv::Pose>("A pose to be plotted"));
    }

    static void initOutputs(dv::OutputDefinitionList& out) {
        out.addFrameOutput("trajectory");
    }

    static const char *initDescription() {
        return "3D trajectory preview in 2D from pose points.";
    }

    PoseViewerModule() : imageSize(cv::Size(defaultWidth, defaultHeight)), viewer(imageSize),
                         drawThread(&PoseViewerModule::drawLoop, this), shouldTerminate(false) {
    	const auto poseInput = inputs.getVectorInput<dv::Pose, dv::Pose>("pose");
        outputs.getFrameOutput("trajectory").setup(imageSize.width, imageSize.height, poseInput.getOriginDescription());

    }

    virtual ~PoseViewerModule() {
        stopDrawLoop();
    }

    static void initConfigOptions(dv::RuntimeConfig& config) {
        config.add("imageWidth",
                   dv::ConfigOption::intOption("Width of the displayed frame in pixels", defaultWidth, 0, 1280));
        config.add("imageHeight",
                   dv::ConfigOption::intOption("Height of the displayed frame in pixels", defaultHeight, 0, 960));
        config.add("lineThickness", dv::ConfigOption::intOption("Thickness of the lines on the drawing", 1, 1, 10));

        config.add("camX", dv::ConfigOption::intOption("Camera position X coordinate [m]", 0, -100, 100));
        config.add("camY", dv::ConfigOption::intOption("Camera position Y coordinate [m]", 0, -100, 100));
        config.add("camZ", dv::ConfigOption::intOption("Camera position Z coordinate [m]", -5, -100, 100));
        config.add("camYaw", dv::ConfigOption::intOption("Camera orientation yaw angle [deg]", 0, -180, 180));
        config.add("camPitch", dv::ConfigOption::intOption("Camera orientation pitch angle [deg]", 0, -180, 180));
        config.add("camRoll", dv::ConfigOption::intOption("Camera orientation roll angle [deg]", 0, -180, 180));
        config.add("frameSize", dv::ConfigOption::intOption("Corrdinate frame size [m]", 5, 1, 20));
        config.add("viewMode", dv::ConfigOption::listOption("Mode in which the plot is displayed.", "custom",
                                                            {"custom", "XY", "YZ", "ZX", "XZ", "YX", "ZY"}, false));
        config.add("gridPlane",
                   dv::ConfigOption::listOption("Plane on which the grid is shown", "XY", {"none", "XY", "YZ", "ZX"},
                                                false));
        config.add("reset", dv::ConfigOption::buttonOption("Reset history", "Reset"));
    }

    void configUpdate() override {


        const std::string viewMode = config.getString("viewMode");
        std::unique_lock<std::mutex> lock(viewerMutex);
        if (config.getBool("reset")) {
            viewer.reset();
            config.setBool("reset", false);
        }

        if (viewMode == "custom") {
            const auto cameraPosition = Eigen::Vector3f(static_cast<const float>(config.getInt("camX")),
                                                        static_cast<const float>(config.getInt("camY")),
                                                        static_cast<const float>(config.getInt("camZ")));
            viewer.updateCameraPosition(cameraPosition);
            viewer.updateCameraOrientation(static_cast<float>(config.getInt("camYaw")),
                                           static_cast<float>(config.getInt("camPitch")),
                                           static_cast<float>(config.getInt("camRoll")));
        }
        viewer.setViewMode(viewMode);
        viewer.setGridPlane(config.getString("gridPlane"));

        const auto newSize = cv::Size(config.getInt("imageWidth"), config.getInt("imageHeight"));
        viewer.updateImageSize(newSize);
        viewer.updateLineThickness(config.getInt("lineThickness"));

        viewer.updateFrameSize(static_cast<float>(config.getInt("frameSize")));
    }

    void draw() {
        std::unique_lock<std::mutex> lock(viewerMutex);
        auto frameout = outputs.getFrameOutput("trajectory");
        auto image = viewer.getImage();
        frameout << viewer.getTimestamp() << image << dv::commit;
    }

    void drawLoop() {
        while (!shouldTerminate.load()) {
            draw();
            std::this_thread::sleep_for(std::chrono::milliseconds(30));
        }
    }

    void stopDrawLoop() {
        if (!shouldTerminate.load()) {
            shouldTerminate.store(true);
            drawThread.join();
        }
    }

    void run() override {
    	auto pose = inputs.getInput<dv::Pose>("pose").data();
		const auto position = Eigen::Vector3f(pose->translation.x(), pose->translation.y(), pose->translation.z());
		const auto orientation = Eigen::Quaternionf(pose->rotation.w(), pose->rotation.x(), pose->rotation.y(),
													pose->rotation.z());
		std::unique_lock<std::mutex> lock(viewerMutex);
		viewer.addPose(position, orientation, pose->timestamp);
    }

};

registerModuleClass(PoseViewerModule)

