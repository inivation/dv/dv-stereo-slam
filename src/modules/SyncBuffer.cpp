//
// Created by rokas on 2020-10-02.
//

#include <functional>

#include <dv-sdk/module.hpp>
#include <boost/circular_buffer.hpp>


class SyncBuffer : public dv::ModuleBase {

typedef dv::InputDataWrapper<dv::Frame> DvFrame;
typedef boost::circular_buffer<DvFrame> FrameBuffer;

protected:
    FrameBuffer frameBuffer;

public:
    static void initInputs(dv::InputDefinitionList &in) {
        in.addTriggerInput("sync", true);
        in.addFrameInput("frames");
    }

    static void initOutputs(dv::OutputDefinitionList &out) {
        out.addFrameOutput("frame");
        out.addTriggerOutput("sync");
    }

    static const char *initDescription() {
        return "Buffers input frames / events, passes through nearest frame / event array to the trigger timestamp";
    }

    static void initConfigOptions(dv::RuntimeConfig &config) {
        config.add("bufferSize",
                   dv::ConfigOption::intOption("Size of the circular buffer", 10, 1, 1000));
    }

    void configUpdate() override {
        uint32_t bufferSize = static_cast<uint32_t>(config.getInt("bufferSize"));
        frameBuffer = boost::circular_buffer<DvFrame>(bufferSize);
    }

    SyncBuffer() {
        outputs.getFrameOutput("frame").setup(inputs.getFrameInput("frames"));
        outputs.getTriggerOutput("sync").setup("Time synchronization output");
    }

    static FrameBuffer::const_iterator findClosest(const FrameBuffer& buffer, int64_t timestamp) {
        int64_t minDistance = INT64_MAX;
        auto iter = buffer.begin();
        auto minIter = buffer.end();
        while (iter != buffer.end()) {
            int64_t timedistance = std::abs(iter->timestamp() - timestamp);
            if (timedistance < minDistance) {
                minIter = iter;
                minDistance = timedistance;
            }
            iter++;
        }
        return minIter;
    }

    void run() override {
        auto frameIn = inputs.getFrameInput("frames");
        if (auto frame = frameIn.frame()) {
            frameBuffer.push_back(frame);
        }

        auto sync = inputs.getTriggerInput("sync");
        if (sync.isConnected()) {
            if (auto trigger = sync.data()) {
                int64_t timestamp = trigger.front().timestamp;
                if (!frameBuffer.empty()) {
                    auto frame = findClosest(frameBuffer, timestamp);
                    outputs.getFrameOutput("frame") << timestamp << *frame->getMatPointer() << dv::commit;
                    outputs.getTriggerOutput("sync") << trigger << dv::commit;
                }
            }
        } else if (frameBuffer.full()) {
            auto& lastEntry = frameBuffer.front();
            int64_t timestamp = lastEntry.timestamp();
            outputs.getFrameOutput("frame") << timestamp << *lastEntry.getMatPointer() << dv::commit;
            dv::Trigger trigger;
            trigger.timestamp = timestamp;
            trigger.type = dv::TriggerType::TIMESTAMP_RESET;
            outputs.getTriggerOutput("sync") << trigger << dv::commit;
        }
    }

};

registerModuleClass(SyncBuffer)
