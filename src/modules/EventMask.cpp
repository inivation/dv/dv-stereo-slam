//
// Created by rokas on 2021-02-08.
//

//
// Created by rokas on 2020-11-27.
//

#include <functional>

#include <dv-sdk/module.hpp>
#include <opencv2/opencv.hpp>

#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

class EventMask : public dv::ModuleBase {
private:
    cv::Mat mask;
    cv::Size imageSize;

public:
    static void initInputs(dv::InputDefinitionList &in) {
        in.addEventInput("events");
    }

    static void initOutputs(dv::OutputDefinitionList &out) {
        out.addEventOutput("events");
    }

    static void initConfigOptions(dv::RuntimeConfig &config) {
        config.add("maskImage",
                   dv::ConfigOption::fileOpenOption("Mask image file", "png,jpg,jpeg,bmp"));
    }

    static const char *initDescription() {
        return "Filter events matching mask image.";
    }

    void configUpdate() override {
        std::string maskPath = config.getString("maskImage");
        if (fs::exists(maskPath)) {
            mask = cv::imread(maskPath, cv::IMREAD_GRAYSCALE);
            if (mask.size() != imageSize) {
                throw std::runtime_error("Mask size is not equal to input image dimensions!");
            }
        } else {
            throw std::runtime_error("Image file is not available [" + maskPath + "]");
        }
    }

    EventMask() {
        auto inputFrame = inputs.getEventInput("events");
        outputs.getEventOutput("events").setup(inputFrame);
        imageSize = cv::Size(inputFrame.sizeX(), inputFrame.sizeY());
    }

    void run() override {
        auto input = inputs.getEventInput("events");
        auto output =  outputs.getEventOutput("events").events();
        if (auto inputEvents = input.data()) {
            output.reserve(inputEvents.size());
            for (const dv::Event& event : inputEvents) {
                if (mask.at<uint8_t>(event.y(), event.x())) {
                    output.emplace_back(event);
                }
            }
            output.shrink_to_fit();
            if (!output.empty()) {
                output.commit();
            }
        }
    }

};

registerModuleClass(EventMask)

