//
// Created by rokas on 2020-10-26.
//

#include <functional>

#include <dv-sdk/module.hpp>
#include <dv-sdk/processing/core.hpp>
#include <opencv2/imgproc.hpp>
#include <boost/circular_buffer.hpp>

class SyncBuffer : public dv::ModuleBase {

    typedef dv::InputDataWrapper<dv::Frame> DvFrame;
    typedef boost::circular_buffer<DvFrame> FrameBuffer;

protected:
    dv::EventStore events;
    FrameBuffer frameBuffer;

    int64_t timeWindow = 1000;

public:
    static void initInputs(dv::InputDefinitionList &in) {
        in.addEventInput("events");
        in.addFrameInput("frame");
    }

    static void initOutputs(dv::OutputDefinitionList &out) {
        out.addFrameOutput("overlay");
    }

    static const char *initDescription() {
        return "Overlays events on top of an image";
    }

    static void initConfigOptions(dv::RuntimeConfig &config) {
        config.add("time_window",
                   dv::ConfigOption::longOption("Time window interval in microseconds to slice events when drawing on the image ", 1000, 0, 1000000));
    }

    void configUpdate() override {
        timeWindow = config.getLong("time_window");
    }

    SyncBuffer() {
        auto frameIn = inputs.getFrameInput("frame");
        outputs.getFrameOutput("overlay").setup(frameIn.sizeX(), frameIn.sizeY(), frameIn.getOriginDescription());

        frameBuffer = FrameBuffer(3);
    }

    void run() override {
        auto eventInput = inputs.getEventInput("events");
        if (auto eventList = eventInput.events()) {
            events.add(dv::EventStore(eventList));
        }
        auto frameIn = inputs.getFrameInput("frame");
        if (auto frame = frameIn.frame()) {
            frameBuffer.push_back(frame);
            if (frameBuffer.full()) {
                auto& drawFrame = frameBuffer.front();
                auto slice = events.sliceTime(drawFrame.timestamp() - timeWindow, drawFrame.timestamp());
                cv::Mat overlay;
                cv::Mat originFrame = *drawFrame.getMatPointer();
                bool singleChannelInput = originFrame.channels() == 1;
                if (singleChannelInput) {
                    cv::cvtColor(originFrame, overlay, cv::COLOR_GRAY2BGR);
                } else {
                    overlay = originFrame;
                }
                cv::Vec3b green = {0, 255, 0};
                cv::Vec3b red = {0, 0, 255};
                // TODO: sort events on coordinate axis to reduce cache misses
                for (const auto& event : slice) {
                    cv::Point2i coords(event.x(), event.y());
                    uint8_t maskValue = originFrame.at<uint8_t>(coords);
                    if (maskValue > 0) {
                        overlay.at<cv::Vec3b>(coords) = green;
                    } else {
                        overlay.at<cv::Vec3b>(coords) = red;
                    }
                }
                if (slice.isEmpty()) {
                    log.warning << "No events available for the frame";
                } else {
                    auto frameOut = outputs.getFrameOutput("overlay");
                    frameOut << drawFrame.timestamp() << overlay << dv::commit;
                }

            }
        }
    }

};

registerModuleClass(SyncBuffer)
