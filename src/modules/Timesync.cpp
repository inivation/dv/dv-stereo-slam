//
// Created by rokas on 2020-10-01.
//

#include <functional>

#include <dv-sdk/module.hpp>
#include <boost/circular_buffer.hpp>
#include <chrono>
#include <dv-sdk/processing/core.hpp>

class Timesync : public dv::ModuleBase {
private:
    boost::circular_buffer<int64_t> queue;
    int64_t lastExecutionTime = -1;
    int64_t skipTime = -1;

public:
    static void initInputs(dv::InputDefinitionList &in) {
        in.addEventInput("events", true);
    }

    static void initOutputs(dv::OutputDefinitionList &out) {
        out.addTriggerOutput("trigger");
    }

    static const char *initDescription() {
        return ("Generates a fixed interval timestamp output for synchronization of modules.");
    }

    static void initConfigOptions(dv::RuntimeConfig &config) {
        config.add("delayQueueSize",
                   dv::ConfigOption::intOption("Delay queue size", 3, 1, 32*15));
        config.add("frequency",
                   dv::ConfigOption::intOption("Triggering frequency in Hz", 100, 1, 1000));
    }

    Timesync() {
        outputs.getTriggerOutput("trigger").setup("Timesync");
    }

    void configUpdate() override {
        int queueSize = config.getInt("delayQueueSize");
        queue = boost::circular_buffer<int64_t>(static_cast<unsigned long>(queueSize));
        skipTime = 1000000LL / static_cast<int64_t>(config.getInt("frequency"));
        ModuleBase::configUpdate();
    }

    static int64_t timestampNow() {
        using namespace std::chrono;
        high_resolution_clock::time_point t1 = high_resolution_clock::now();
        return t1.time_since_epoch().count() / 1000LL;
    }

    bool handleTimestamp(int64_t timestamp) {
        int64_t diff = timestamp - lastExecutionTime;
        if (diff >= skipTime) {
            queue.push_back(timestamp);
            lastExecutionTime = timestamp;
            if (queue.full()) {
                auto output = outputs.getTriggerOutput("trigger");
                dv::Trigger trigger;
                trigger.timestamp = queue.front();
                trigger.type = dv::TriggerType::TIMESTAMP_RESET;
                output << trigger << dv::commit;
                return true;
            }
        }
        return false;
    }

    void run() override {
        auto evinput = inputs.getEventInput("events");
        int64_t timestamp;
        if (evinput.isConnected() && !evinput.events().empty()) {
            auto events = evinput.events();
            size_t increment = events.size() / 20;
            if (increment == 0) {
                increment = 1;
            }
            auto iter = events.begin();
            while (iter < events.end()) {
                handleTimestamp(iter->timestamp());
                iter += increment;
            }
        } else {
            timestamp = timestampNow();
            if (!handleTimestamp(timestamp)) {
                usleep(10);
            }
        }
    }
};

registerModuleClass(Timesync)
