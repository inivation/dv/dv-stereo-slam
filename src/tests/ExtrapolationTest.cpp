//
// Created by rokas on 2020-10-30.
//


#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN  // in only one cpp file
#include <boost/test/unit_test.hpp>

#include <Transformer.h>


BOOST_AUTO_TEST_CASE( my_test ) {
    // Test extrapolation and interpolation in between only using translational vectors
    // quaternion math is assumed to be correct (it must be)

    kindr::minimal::RotationQuaternion noRotation(0., 0., 0., 1.);
    kindr::minimal::Position p1(1., 1., 1.);
    kindr::minimal::Position p2(2., 2., 2.);
    Transformation T1(p1, noRotation), T2(p2, noRotation);

    Transformation extrapolated = T2 * T1.inverse() * T2;

    auto position = extrapolated.getPosition();

    BOOST_CHECK( position.x() == 3. );
    BOOST_CHECK( position.y() == 3. );
    BOOST_CHECK( position.z() == 3. );

    Transformer transformer;
    transformer.pushTransform(1000, T1);
    transformer.pushTransform(2000, T2);
    transformer.pushTransform(3000, extrapolated);

    Transformation interpolatedInFuture;
    BOOST_CHECK( transformer.getTransformAt(2500, interpolatedInFuture) );

    auto interpolated = interpolatedInFuture.getPosition();
    BOOST_CHECK( interpolated.x() == 2.5 );
    BOOST_CHECK( interpolated.y() == 2.5 );
    BOOST_CHECK( interpolated.z() == 2.5 );
}
