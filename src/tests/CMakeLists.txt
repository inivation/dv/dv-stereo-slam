set (Boost_USE_STATIC_LIBS OFF)
find_package (Boost REQUIRED COMPONENTS unit_test_framework)
find_package (Eigen3 3.3 REQUIRED NO_MODULE)

include_directories (
        ${Boost_INCLUDE_DIRS}
        ${EIGEN3_INCLUDE_DIR}
        ../../include/dv-slam/utilities
        ../../ESVO/esvo_core/include
        ../../thirdparty/minkindr/minkindr/include
        ../../include)
find_package(GLOG REQUIRED)
include_directories(${GLOG_INCLUDE_DIRS})

SET(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}
        ${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_DATAROOTDIR}/dv /usr/${CMAKE_INSTALL_DATAROOTDIR}/dv
        ${CMAKE_INSTALL_PREFIX}/share/dv /usr/share/dv
        /usr/local/${CMAKE_INSTALL_DATAROOTDIR}/dv /usr/local/share/dv)

# Basic setup
INCLUDE(dv-modules)
cmake_policy(SET CMP0063 NEW)

add_executable (ExtrapolationTest ExtrapolationTest.cpp)
target_link_libraries (ExtrapolationTest ${Boost_LIBRARIES} ${GLOG_LIBRARIES} ${DV_LIBRARIES})

add_executable (PoseViewerTest PoseViewerTest.cpp ../../include/dv-slam/utilities/PoseViewer.h)
target_link_libraries (PoseViewerTest ${Boost_LIBRARIES} ${OpenCV_LIBS} ${GLOG_LIBRARIES})
