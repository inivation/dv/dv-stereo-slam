//
// Created by rokas on 2020-10-07.
//

#include <dv-slam/utilities/Utils.h>
#include <dv-slam/slam/ESVO/ESVOStandAloneTracker.h>

#include <utility>

#define ESVO_CORE_TRACKING_DEBUG 0

using namespace esvo_core;
using namespace esvo_core::core;

ESVOStandAloneTracker::ESVOStandAloneTracker(std::string  calibration) :
        calibInfoDir_(std::move(calibration)),
        camSysPtr_(new CameraSystem(calibInfoDir_, false)),
        events_left_(5000000),
        rpType_(RegProblemType::REG_ANALYTICAL),
        ets_(IDLE),
        ESVO_System_Status_(INITIALIZATION),
        rpConfigPtr_(new RegProblemConfig(
                1,
                1,
                5,
                std::string("Huber"),
                50.0,
                0.15,
                2.,
                1.0,
                1000,
                2000,
                200,
                10)),
        rpSolver_(camSysPtr_, rpConfigPtr_, rpType_, NUM_THREAD_TRACKING) {
    // offline data
    dvs_frame_id_ = std::string("dvs");
    world_frame_id_ = std::string("world");
    TS_id_ = 0;
    /**** online parameters ***/
    TS_HISTORY_LENGTH_ = 50;
    REF_HISTORY_LENGTH_ = 5;
    bSaveTrajectory_ = false;
    bVisualizeTrajectory_ = false;
    resultPath_ = std::string();

    /*** Tracker ***/
    T_world_cur_ = Eigen::Matrix<double, 4, 4>::Identity();
}

void ESVOStandAloneTracker::pushEvents(const shared_ptr<const dv::EventPacket> &eventsPtr) {
    std::lock_guard<std::mutex> lockGuard(data_mutex_);
    std::copy(eventsPtr->elements.begin(), eventsPtr->elements.end(), std::back_inserter(events_left_));
}

void ESVOStandAloneTracker::pushFrame(cv::Mat &left, int64_t t_new_TS) {
    std::lock_guard<std::mutex> lockGuard(data_mutex_);
    auto ts = TimeSurfaceObservation(left, TS_id_);
    ts.getTimeSurfaceNegative(rpConfigPtr_->kernelSize_, rpConfigPtr_->contrast_);
    ts.computeTsNegativeGrad();
    TS_history_.emplace(t_new_TS, ts);
    TS_id_++;

    // keep TS_history_'s size constant
    while (TS_history_.size() > TS_HISTORY_LENGTH_) {
        auto it = TS_history_.begin();
        TS_history_.erase(it);
    }
}

void ESVOStandAloneTracker::pushReferencePointCloud(const PointCloud &pointCloud, int64_t t_new_TS) {
    std::lock_guard<std::mutex> lockGuard(data_mutex_);
    refPCMap_.emplace(t_new_TS, pointCloud);
    while (refPCMap_.size() > REF_HISTORY_LENGTH_) {
        auto it = refPCMap_.begin();
        refPCMap_.erase(it);
    }
}

void ESVOStandAloneTracker::refDataTransferring() {
    std::lock_guard<std::mutex> lockGuard(data_mutex_);
    // load reference info
    auto r_begin = refPCMap_.rbegin();
    if (r_begin != refPCMap_.rend() && r_begin->first > ref_.t_) {
        ref_.t_ = r_begin->first;

        if (ESVO_System_Status_ == INITIALIZATION && ets_ == IDLE) {
            ref_.tr_.setIdentity();
        } else if (ESVO_System_Status_ == SYSTEM_WORKING || (ESVO_System_Status_ == INITIALIZATION && ets_ == WORKING)) {
            if (!tf_.getTransformAt(ref_.t_, ref_.tr_)) {
                return;
            }
        }

        ref_.vPointXYZPtr_ = r_begin->second;
    }

}

bool ESVOStandAloneTracker::curDataTransferring(int64_t timestamp) {
    std::lock_guard<std::mutex> lockGuard(data_mutex_);
    // load current observation
    //auto ev_last_it = EventBuffer_lower_bound(events_left_, cur_.t_);
    auto TS_it = TS_history_.find(timestamp);
    if (TS_it == TS_history_.end() || cur_.t_ == TS_it->first || timestamp < ref_.t_) {
        return false;
    }
    cur_.t_ = TS_it->first;
    cur_.pTsObs_ = &TS_it->second;

    if (ESVO_System_Status_ == INITIALIZATION && ets_ == IDLE) {
        cur_.tr_ = ref_.tr_;
        ESVO_System_Status_ = SYSTEM_WORKING;
    }
    if (ESVO_System_Status_ == SYSTEM_WORKING || (ESVO_System_Status_ == INITIALIZATION && ets_ == WORKING)) {
        cur_.tr_ = Transformation(T_world_cur_);
    }
    // Count the number of events occuring since the last observation.
    //auto ev_cur_it = EventBuffer_lower_bound(events_left_, cur_.t_);
    cur_.numEventsSinceLastObs_ = 10000; //static_cast<size_t>(std::distance(ev_last_it, ev_cur_it)) + 1UL;
    return true;
}

bool ESVOStandAloneTracker::track() {

// create new regProblem
#if  ESVO_CORE_TRACKING_DEBUG
    double t_resetRegProblem, t_solve, t_pub_result, t_pub_gt;
    TicToc tt;
    tt.tic();
#endif
    if (rpSolver_.resetRegProblem(&ref_, &cur_)) {
#if  ESVO_CORE_TRACKING_DEBUG
        t_resetRegProblem = tt.toc();
        tt.tic();
#endif
        if (ets_ == IDLE)
            ets_ = WORKING;
        if (rpType_ == REG_NUMERICAL)
            rpSolver_.solve_numerical();
        if (rpType_ == REG_ANALYTICAL)
            rpSolver_.solve_analytical();
#if ESVO_CORE_TRACKING_DEBUG
        t_solve = tt.toc();
        tt.tic();
#endif
        T_world_cur_ = cur_.tr_.getTransformationMatrix();
        tf_.pushTransform(cur_.t_, cur_.tr_);
#if ESVO_CORE_TRACKING_DEBUG
        t_pub_result = tt.toc();
        double t_overall_count = t_resetRegProblem + t_solve + t_pub_result;
        LOG(INFO) << "\n";
        LOG(INFO) << "------------------------------------------------------------";
        LOG(INFO) << "--------------------Tracking Computation Cost---------------";
        LOG(INFO) << "------------------------------------------------------------";
        LOG(INFO) << "ResetRegProblem: " << t_resetRegProblem << " ms, (" << t_resetRegProblem / t_overall_count * 100 << "%).";
        LOG(INFO) << "Registration: " << t_solve << " ms, (" << t_solve / t_overall_count * 100 << "%).";
        LOG(INFO) << "pub result: " << t_pub_result << " ms, (" << t_pub_result / t_overall_count * 100 << "%).";
        LOG(INFO) << "Total Computation (" << rpSolver_.lmStatics_.nPoints_ << "): " << t_overall_count << " ms.";
        LOG(INFO) << "------------------------------------------------------------";
        LOG(INFO) << "------------------------------------------------------------";
        static double total_sum = 0.0;
        static uint64_t counter = 0;
        counter += 1;
        total_sum += tt.toc();
        if (counter >= 50) {
            double avg = total_sum / (double) counter;
            total_sum = 0.0;
            counter = 0;
            std::cout << avg << std::endl;
        }
#endif
        return true;
    } else {
        return false;
    }
}

std::pair<int64_t, Transformation> ESVOStandAloneTracker::getWorldTransform() const {
    return std::make_pair(cur_.t_, cur_.tr_);
}

cv::Mat ESVOStandAloneTracker::visualizeTracking() {
    return rpSolver_.visualize();
}

void ESVOStandAloneTracker::setMinInvDepthRange(double minInvDepth) {
    rpConfigPtr_->invDepth_min_range_ = minInvDepth;
}

void ESVOStandAloneTracker::setMaxInvDepthRange(double maxInvDepth) {
    rpConfigPtr_->invDepth_max_range_ = maxInvDepth;
}

void ESVOStandAloneTracker::setContrast(double contrast) {
    rpConfigPtr_->contrast_ = contrast;
}
