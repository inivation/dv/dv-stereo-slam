//
// Created by rokas on 2020-12-23.
//

#include <dv-slam/utilities/TimeProfiler.h>
#include <pcl/filters/voxel_grid.h>

#include <boost/make_shared.hpp>

#include <dv-slam/slam/ESVO/ESVOFuser.h>

#define SLAM_FUSION_DEBUG 0

slam::ESVOFuser::ESVOFuser(esvo_core::container::CameraSystem::Ptr &camSysPtr,
                           esvo_core::core::DepthProblemConfig::LS_Norm norm) {
    dFusor_ = std::make_shared<esvo_core::core::DepthFusion>(camSysPtr, norm);
    dRegularizor_ = std::make_shared<esvo_core::core::DepthRegularization>(norm);
    frameWidth = camSysPtr->cam_left_ptr_->width_;
    frameHeight = camSysPtr->cam_left_ptr_->height_;
#if PCL_MAJOR_VERSION >= 1 && PCL_MINOR_VERSION >= 11
		pc_global = std::make_shared<esvo_core::PointCloud>();
#else
		pc_global = boost::make_shared<esvo_core::PointCloud>();
#endif
}

esvo_core::container::DepthFrame::Ptr slam::ESVOFuser::fuse(const slam::SparseDepthFramePtr &sparsePoints) {
    esvo_core::container::DepthFrame::Ptr depthFramePtr_new = std::make_shared<esvo_core::container::DepthFrame>(
            frameHeight, frameWidth
            );
    depthFramePtr_new->setTransformation(sparsePoints->transform);
    depthFramePtr_new->setId(frameCounter++);
    frameHistory.insert(std::make_pair(sparsePoints->timestamp, sparsePoints->depthPoints));

    if (sparsePoints->timestamp < lastFusionTimestamp) {
        return nullptr;
    }
    lastFusionTimestamp = sparsePoints->timestamp;

    if (frameHistory.size() > frameCount) {
        frameHistory.erase(frameHistory.begin());
    }

    // Clear overhead points
    size_t numFusionPoints = 0;
    if (fusionStrategy == ConstPoints) {
        for (const auto &dqvDepthPoint : frameHistory) {
            numFusionPoints += dqvDepthPoint.second->size();
        }
        while (numFusionPoints > 1.5 * pointCount) {
            frameHistory.erase(frameHistory.begin());
            numFusionPoints = 0;
            for (auto &dqvDepthPoint : frameHistory) {
                numFusionPoints += dqvDepthPoint.second->size();
            }
        }
    }

#if SLAM_FUSION_DEBUG
    std::cout << "Fusing " << numFusionPoints << " points" << std::endl;
#endif

    if (initiatilized) {
        auto iter = frameHistory.rbegin();
        while (iter != frameHistory.rend()) {
            dFusor_->update(*(iter->second), depthFramePtr_new);
            iter++;
        }
    } else {
        auto iter = frameHistory.rbegin();
        while (iter != frameHistory.rend()) {
            dFusor_->naive_propagation(*(iter->second), depthFramePtr_new);
            iter++;
        }
        initiatilized = true;
    }
#if SLAM_FUSION_DEBUG
    std::cout << "dMap before clean " << depthFramePtr_new->dMap_->size() << std::endl;
#endif

    depthFramePtr_new->dMap_->clean(pow(stdVar_vis_threshold_, 2), 0, invDepth_max_range_,
                                    invDepth_min_range_);

#if SLAM_FUSION_DEBUG
    std::cout << "dMap after clean " << depthFramePtr_new->dMap_->size() << std::endl;
#endif

    if (regularization) {
        dRegularizor_->apply(depthFramePtr_new->dMap_);
    }

    return depthFramePtr_new;
}


void slam::ESVOFuser::reset() {
    frameHistory.clear();
    initiatilized = false;
    frameCounter = 0;
}

void slam::ESVOFuser::setPointCount(size_t pointCount) {
    ESVOFuser::pointCount = pointCount;
}

void slam::ESVOFuser::setAgeVisThreshold(double ageVisThreshold) {
    age_vis_threshold_ = ageVisThreshold;
}

void slam::ESVOFuser::setInvDepthMaxRange(double invDepthMaxRange) {
    invDepth_max_range_ = invDepthMaxRange;
}

void slam::ESVOFuser::setInvDepthMinRange(double invDepthMinRange) {
    invDepth_min_range_ = invDepthMinRange;
}

void slam::ESVOFuser::setStdVarVisThreshold(double stdVarVisThreshold) {
    stdVar_vis_threshold_ = stdVarVisThreshold;
}

const esvo_core::PointCloud::Ptr &slam::ESVOFuser::updateGlobalPointCloud(const esvo_core::container::DepthFrame::Ptr& depthFrame) {
#if PCL_MAJOR_VERSION >= 1 && PCL_MINOR_VERSION >= 11
	esvo_core::PointCloud::Ptr pc_near_ = std::make_shared<esvo_core::PointCloud>();
#else
	esvo_core::PointCloud::Ptr pc_near_ = boost::make_shared<esvo_core::PointCloud>();
#endif
    pc_near_->reserve(depthFrame->dMap_->size());

    for (const auto &it : *depthFrame->dMap_) {
        if (it.p_cam().norm() < visualize_range_) {
            Eigen::Vector3d p_world = depthFrame->T_world_frame_.transform(it.p_cam());
            pcl::PointXYZ p(
                    static_cast<float>(p_world(0)),
                    static_cast<float>(p_world(1)),
                    static_cast<float>(p_world(2))
            );
            pc_near_->push_back(p);
        }
    }

    esvo_core::PointCloud pc_filtered;
    pcl::VoxelGrid<pcl::PointXYZ> sor;
    sor.setInputCloud(pc_near_);
    sor.setLeafSize(0.03f, 0.03f, 0.03f);
    sor.filter(pc_filtered);

    pc_global->insert(pc_global->end(), pc_filtered.begin(), pc_filtered.end());

    return pc_global;
}
