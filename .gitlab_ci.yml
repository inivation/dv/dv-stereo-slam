variables:
  GIT_SUBMODULE_STRATEGY: normal
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}"
  PROJECT_NAME: "stereo-slam"

default:
  interruptible: true
  tags:
    - docker
    - ubuntu
    - build

.version_name: &version_name
  - export VERSION_NAME=$([[ -z "${RELEASE_VERSION}"  ]] && echo "${CI_COMMIT_TAG}" || echo "${RELEASE_VERSION}")

.change_build_dir: &change_build_dir
  - rm -Rf build
  - mkdir build
  - cd build

.ubuntu_curr_builder: &ubuntu_curr_builder
  stage: build
  image: registry.gitlab.com/inivation/infra/docker-files/ubuntu:rolling
  # Variables is not getting merged, so we use before_script to set CCACHE vars.
  before_script:
    - export CCACHE_DIR="${CI_PROJECT_DIR}/.ccache"
    - export CCACHE_BASEDIR="${CI_PROJECT_DIR}"
    - export CCACHE_COMPILERCHECK="content"
    - apt-get -y install libcaer-dev dv-gui dv-runtime-dev libpcl-dev libyaml-cpp-dev libgoogle-glog-dev liboctomap-dev
  script:
    - *change_build_dir
    - cmake -DCMAKE_INSTALL_PREFIX=/usr ..
    - make -j4 -s
  rules:
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_COMMIT_BRANCH
  cache:
    key: "${CI_JOB_NAME}-${CI_COMMIT_REF_SLUG}"
    paths:
      - .ccache
    policy: pull-push

build_macos_intel:
  stage: build
  tags:
    - macos
    - build
    - x86_64
  variables:
    CCACHE_DIR: "${CI_PROJECT_DIR}/.ccache"
    CCACHE_BASEDIR: "${CI_PROJECT_DIR}"
    CCACHE_COMPILERCHECK: "content"
    PATH: "/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Library/Apple/usr/bin"
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Release"
    CC: clang-llvm
    CXX: clang++-llvm
  script:
    - *change_build_dir
    - arch -x86_64 /usr/local/bin/cmake -DCMAKE_INSTALL_PREFIX=/usr/local ..
    - make -j4
    - if [[ "${CI_PROJECT_NAME}" != "${PROJECT_NAME_INTERNAL}" ]] ; then exit 0; fi
    - if [[ "${CI_COMMIT_BRANCH}" != "master" ]] ; then exit 0; fi
    - rm -Rf /usr/local/include/${PROJECT_NAME}/ /usr/local/lib/cmake/${PROJECT_NAME}/
    - make install
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
  cache:
    key: "${CI_JOB_NAME}-${CI_COMMIT_REF_SLUG}"
    paths:
      - .ccache
    policy: pull-push

build_macos_arm:
  stage: build
  tags:
    - macos
    - build
    - arm
  variables:
    CCACHE_DIR: "${CI_PROJECT_DIR}/.ccache"
    CCACHE_BASEDIR: "${CI_PROJECT_DIR}"
    CCACHE_COMPILERCHECK: "content"
    PATH: "/opt/homebrew/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Library/Apple/usr/bin"
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Release"
    CC: clang-llvm
    CXX: clang++-llvm
  script:
    - *change_build_dir
    - /opt/homebrew/bin/cmake -DCMAKE_INSTALL_PREFIX=/opt/homebrew ..
    - make -j4
    - make test
    - if [[ "${CI_PROJECT_NAME}" != "${PROJECT_NAME_INTERNAL}" ]] ; then exit 0; fi
    - if [[ "${CI_COMMIT_BRANCH}" != "master" ]] ; then exit 0; fi
    - rm -Rf /opt/homebrew/include/${PROJECT_NAME}/ /opt/homebrew/lib/cmake/${PROJECT_NAME}/
    - make install
  rules:
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_COMMIT_BRANCH
  cache:
    key: "${CI_JOB_NAME}-${CI_COMMIT_REF_SLUG}"
    paths:
      - .ccache
    policy: pull-push

build_windows:
  stage: build
  tags:
    - windows
    - build
  variables:
    MSYSTEM: "MINGW64"
    CCACHE_DIR: "${CI_PROJECT_DIR}/.ccache"
    CCACHE_BASEDIR: "${CI_PROJECT_DIR}"
    CCACHE_COMPILERCHECK: "content"
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Release"
    CC: gcc
    CXX: g++
  script:
    - source /usr/bin/init-paths
    - *change_build_dir
    - cmake -G "MSYS Makefiles" -DCMAKE_INSTALL_PREFIX=/mingw64 ..
    - make -j8
    - make test
    - if [[ "${CI_PROJECT_NAME}" != "${PROJECT_NAME_INTERNAL}" ]] ; then exit 0; fi
    - if [[ "${CI_COMMIT_BRANCH}" != "master" ]] ; then exit 0; fi
    - rm -Rf /mingw64/include/${PROJECT_NAME}/ /mingw64/lib/cmake/${PROJECT_NAME}/
    - make install
  rules:
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_COMMIT_BRANCH
  cache:
    key: "${CI_JOB_NAME}-${CI_COMMIT_REF_SLUG}"
    paths:
      - .ccache
    policy: pull-push

basic_syntax_check_clang:
  variables:
    # Oldest supported clang compiler.
    # Must be clang 15 as older have compile issues with libstdc++-13.
    CC: clang-15
    CXX: clang++-15
  <<: *ubuntu_curr_builder
  script:
    # Generate header files.
    - cmake -DCMAKE_INSTALL_PREFIX=/usr .
    # Check syntax.
    - find include/ -type f -iname '*.hpp' | sort -u > headers.txt
    - find include/ -type f -iname '*.h' | sort -u >> headers.txt
    - for h in $(cat headers.txt) ; do echo "\nChecking === ${h}\n\n" ; ${CXX} -std=c++20 -O2 -pipe "${h}" $(pkg-config --cflags opencv4) $(pkg-config --cflags eigen3) ; rm "${h}.gch" ; done

build_ubuntu_1804_release:
  variables:
    CC: gcc-10
    CXX: g++-10
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Release"
  <<: *ubuntu_curr_builder
  image: registry.gitlab.com/inivation/infra/docker-files/ubuntu:18.04

build_ubuntu_1804_debug:
  variables:
    CC: gcc-10
    CXX: g++-10
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Debug"
  <<: *ubuntu_curr_builder
  image: registry.gitlab.com/inivation/infra/docker-files/ubuntu:18.04

build_ubuntu_2004_release:
  variables:
    CC: gcc-10
    CXX: g++-10
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Release"
  <<: *ubuntu_curr_builder
  image: registry.gitlab.com/inivation/infra/docker-files/ubuntu:20.04

build_ubuntu_2004_debug:
  variables:
    CC: gcc-10
    CXX: g++-10
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Debug"
  <<: *ubuntu_curr_builder
  image: registry.gitlab.com/inivation/infra/docker-files/ubuntu:20.04

build_ubuntu_2204_release:
  variables:
    CC: gcc-12
    CXX: g++-12
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Release"
  <<: *ubuntu_curr_builder
  image: registry.gitlab.com/inivation/infra/docker-files/ubuntu:22.04

build_ubuntu_2204_debug:
  variables:
    CC: gcc-12
    CXX: g++-12
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Debug"
  <<: *ubuntu_curr_builder
  image: registry.gitlab.com/inivation/infra/docker-files/ubuntu:22.04

build_ubuntu_gcc10_release:
  variables:
    CC: gcc-10
    CXX: g++-10
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Release"
  <<: *ubuntu_curr_builder

build_ubuntu_gcc10_debug:
  variables:
    CC: gcc-10
    CXX: g++-10
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Debug"
  <<: *ubuntu_curr_builder

build_ubuntu_gcc11_release:
  variables:
    CC: gcc-11
    CXX: g++-11
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Release"
  <<: *ubuntu_curr_builder

build_ubuntu_gcc11_debug:
  variables:
    CC: gcc-11
    CXX: g++-11
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Debug"
  <<: *ubuntu_curr_builder

build_ubuntu_gcc12_release:
  variables:
    CC: gcc-12
    CXX: g++-12
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Release"
  <<: *ubuntu_curr_builder

build_ubuntu_gcc12_debug:
  variables:
    CC: gcc-12
    CXX: g++-12
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Debug"
  <<: *ubuntu_curr_builder

build_ubuntu_gcc13_release:
  variables:
    CC: gcc-13
    CXX: g++-13
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Release"
  <<: *ubuntu_curr_builder

build_ubuntu_gcc13_debug:
  variables:
    CC: gcc-13
    CXX: g++-13
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Debug"
  <<: *ubuntu_curr_builder

build_ubuntu_clang15_release:
  variables:
    CC: clang-15
    CXX: clang++-15
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Release"
  <<: *ubuntu_curr_builder

build_ubuntu_clang15_debug:
  variables:
    CC: clang-15
    CXX: clang++-15
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Debug"
  <<: *ubuntu_curr_builder

build_ubuntu_clang16_release:
  variables:
    CC: clang-16
    CXX: clang++-16
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Release"
  <<: *ubuntu_curr_builder

build_ubuntu_clang16_debug:
  variables:
    CC: clang-16
    CXX: clang++-16
    CTEST_OUTPUT_ON_FAILURE: 1
    CMAKE_BUILD_TYPE: "Debug"
  <<: *ubuntu_curr_builder

# Run pre-commit on all files to make sure it was run on client.
# This will do all formatting checks in a consistent way.
pre-commit_check:
  stage: build
  image: registry.gitlab.com/inivation/infra/docker-files/ubuntu:rolling
  before_script:
    - pre-commit install
  script:
    - pre-commit run -a
  rules:
    - if: $CI_COMMIT_BRANCH
