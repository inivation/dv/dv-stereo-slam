#include <esvo_core/container/CameraSystem.h>
#include <opencv2/core/eigen.hpp>
#include <glog/logging.h>

#include <boost/filesystem.hpp>
#include <fmt/format.h>

namespace fs = boost::filesystem;

namespace esvo_core {
    namespace container {
        PerspectiveCamera::PerspectiveCamera() {
        }

        PerspectiveCamera::~PerspectiveCamera() {}

        void PerspectiveCamera::setIntrinsicParameters(
                size_t width,
                size_t height,
                std::string &cameraName,
                std::string &distortion_model,
                std::vector<double> &vD,
                std::vector<double> &vK,
                std::vector<double> &vRectMat,
                std::vector<double> &vP) {
            width_ = width;
            height_ = height;
            cameraName_ = cameraName;
            distortion_model_ = distortion_model;
            D_ = Eigen::Matrix<double, 4, 1>(vD.data());
            K_ = Eigen::Matrix<double, 3, 3, Eigen::RowMajor>(vK.data());
            RectMat_ = Eigen::Matrix<double, 3, 3, Eigen::RowMajor>(vRectMat.data());
            P_ = Eigen::Matrix<double, 3, 4, Eigen::RowMajor>(vP.data());
            R_f = P_.block<3, 3>(0, 0).cast<float>();
            T_f = P_.block<3, 1>(0, 3).cast<float>();
            R_d = P_.block<3, 3>(0, 0);
            T_d = P_.block<3, 1>(0, 3);
            P_tilde.block<3, 4>(0, 0) = P_;

            preComputeRectifiedCoordinate();
        }

        void PerspectiveCamera::rectifyImage(const cv::Mat& input, cv::Mat& output) {
            cv::remap(input, output, undistort_map1_, undistort_map2_, cv::INTER_LINEAR);
        }


        void PerspectiveCamera::preComputeRectifiedCoordinate() {
            precomputed_rectified_points_.reserve(height_ * width_);//Eigen::Matrix2Xd(2, height_ * width_);

            cv::Mat_<cv::Point2f> RawCoordinates(1, width_ * height_);
            for (int y = 0; y < height_; y++) {
                for (int x = 0; x < width_; x++) {
                    int index = y * width_ + x;
                    RawCoordinates(index) = cv::Point2f(x, y);
                }
            }

            cv::Mat_<cv::Point2f> RectCoordinates(1, height_ * width_);
            cv::Mat cvKmat(3, 3, CV_64F);
            cv::Mat cvDistCoeff(1, 4, CV_64F);
            cv::Mat cvRectMat(3, 3, CV_64F);
            cv::Mat cvPmat(3, 4, CV_64F);

            cv::eigen2cv(K_, cvKmat);
            cv::eigen2cv(D_, cvDistCoeff);
            cv::eigen2cv(RectMat_, cvRectMat);
            cv::eigen2cv(P_, cvPmat);
            if (distortion_model_ == "plumb_bob") {
                cv::undistortPoints(RawCoordinates, RectCoordinates, cvKmat, cvDistCoeff, cvRectMat, cvPmat);
#if CV_MAJOR_VERSION >= 3
                cv::Size sensor_size(width_, height_);
                cv::initUndistortRectifyMap(cvKmat, cvDistCoeff, cvRectMat, cvPmat,
                                            sensor_size, CV_32FC1, undistort_map1_, undistort_map2_);
                cv::Mat cvSrcMask = cv::Mat::ones(sensor_size, CV_32F);
                cv::Mat cvDstMask = cv::Mat::zeros(sensor_size, CV_32F);
                cv::remap(cvSrcMask, cvDstMask, undistort_map1_, undistort_map2_, cv::INTER_LINEAR);
                cv::threshold(cvDstMask, cvDstMask, 0.999, 255, cv::THRESH_BINARY);
                cvDstMask.convertTo(cvDstMask, CV_8U);
                cv::cv2eigen(cvDstMask, UndistortRectify_mask_);
//    LOG(INFO) << "#################### UndistortRectify_mask_.size: " << UndistortRectify_mask_.size();
#else
                ROS_ERROR_ONCE("You need OpenCV >= 3.0 to use the equidistant camera model.");
                ROS_ERROR_ONCE("Will not publish rectified images.");
#endif
            } else if (distortion_model_ == "equidistant") {
                cv::fisheye::undistortPoints(
                        RawCoordinates, RectCoordinates, cvKmat, cvDistCoeff, cvRectMat, cvPmat);
#if CV_MAJOR_VERSION >= 3
                cv::Size sensor_size(width_, height_);
                cv::fisheye::initUndistortRectifyMap(cvKmat, cvDistCoeff, cvRectMat, cvPmat,
                                                     sensor_size, CV_32FC1, undistort_map1_, undistort_map2_);
                cv::Mat cvSrcMask = cv::Mat::ones(sensor_size, CV_32F);
                cv::Mat cvDstMask = cv::Mat::zeros(sensor_size, CV_32F);
                cv::remap(cvSrcMask, cvDstMask, undistort_map1_, undistort_map2_, cv::INTER_LINEAR);
                cv::threshold(cvDstMask, cvDstMask, 0.1, 255, cv::THRESH_BINARY);
                cvDstMask.convertTo(cvDstMask, CV_8U);
                cv::cv2eigen(cvDstMask, UndistortRectify_mask_);

//    LOG(INFO) << "#################### UndistortRectify_mask_.size: " << UndistortRectify_mask_.size();
#else
                ROS_ERROR_ONCE("You need OpenCV >= 3.0 to use the equidistant camera model.");
                ROS_ERROR_ONCE("Will not publish rectified images.");
#endif
            } else {
                throw std::runtime_error("wrong distortion model is provided.");
            }

            for (size_t i = 0; i < height_ * width_; i++) {
                precomputed_rectified_points_.emplace_back(
                        std::round(RectCoordinates(i).x),
                        std::round(RectCoordinates(i).y)
                        );
            }
        }

        const cv::Point2i&
        PerspectiveCamera::getRectifiedUndistortedCoordinate(uint32_t xcoor, uint32_t ycoor) const {
            return precomputed_rectified_points_.at(ycoor * width_ + xcoor);
        }

        bool PerspectiveCamera::testUndistortionMask(const cv::Point2i& p) const {
            return UndistortRectify_mask_(p.y, p.x) > 0;
        }

        void
        PerspectiveCamera::cam2World(
                const Eigen::Vector2d &x,
                double invDepth,
                Eigen::Vector3d &p) const {
            double z = 1.0 / invDepth;
            Eigen::Matrix<double, 4, 1> x_ss;
            x_ss << x(0), x(1), 1.0, 1.0;
            Eigen::Matrix<double, 4, 4> P_tilde_ = P_tilde;
            P_tilde_(3, 3) = z;
            Eigen::Vector4d p_s = z * P_tilde_.inverse() * x_ss;
            p = p_s.block<3, 1>(0, 0); // / p_s(3); // This normalization by the last element is not necessary.
        }

        void
        PerspectiveCamera::world2Cam(
                const Eigen::Vector3d &p,
                Eigen::Vector2d &x) {
            Eigen::Vector3d x_hom = R_d * p + T_d;
            x.noalias() = x_hom.head(2) / x_hom(2);
        }

        void
        PerspectiveCamera::world2Cam(
                const Eigen::Vector3f &p,
                Eigen::Vector2f&x) {
            Eigen::Vector3f x_hom = R_f * p + T_f;
            x.noalias() = x_hom.head(2) / x_hom(2);
        }

/************************************************************/
/************************************************************/
        CameraSystem::CameraSystem(const std::string &calibInfoDir, bool bPrintCalibInfo) {
            cam_left_ptr_ = std::make_shared<PerspectiveCamera>();
            cam_right_ptr_ = std::make_shared<PerspectiveCamera>();
            if (fs::is_regular_file(calibInfoDir)) {
                loadCalibInfoXML(calibInfoDir, bPrintCalibInfo);
            } else {
                loadCalibInfo(calibInfoDir, bPrintCalibInfo);
            }
            computeBaseline();
        }

        CameraSystem::~CameraSystem() {}

        void CameraSystem::computeBaseline() {
            Eigen::Vector3d temp = cam_right_ptr_->P_.block<3, 3>(0, 0).inverse() *
                                   cam_right_ptr_->P_.block<3, 1>(0, 3);
            baseline_ = temp.norm();
        }

        void CameraSystem::loadCalibInfoXML(const std::string &cameraSystemDir, bool bPrintCalibInfo) {
            cv::FileStorage input;
            input.open(cameraSystemDir, cv::FileStorage::Mode::READ);

            if (!input.isOpened()) {
                throw std::runtime_error(fmt::format("Unable to parse calibration file [{}]", cameraSystemDir));
            }
            std::string leftName, rightName;
            cv::Mat R, T;

            cv::Mat cvD_left, cvK_left, cvRectMat_left, cvP_left;
            cv::Mat cvD_right, cvK_right, cvRectMat_right, cvP_right;
            size_t width, height;

            for (const auto& iter : input.root()) {
                if (leftName.empty()) {
                    leftName = iter.name();
                    cvD_left = iter["distortion_coefficients"].mat();
                    cvK_left = iter["camera_matrix"].mat();
                    width = static_cast<size_t>(iter["image_width"].operator int());
                    height = static_cast<size_t>(iter["image_height"].operator int());
                } else if (rightName.empty()) {
                    rightName = iter.name();
                    cvD_right = iter["distortion_coefficients"].mat();
                    cvK_right = iter["camera_matrix"].mat();

                } else {
                    break;
                }
            }
            R = input["R"].mat();
            T = input["T"].mat();
            T_right_left_.setIdentity();
            Eigen::Matrix<double, 3, 3> R_eigen;// extrinsics
            cv::cv2eigen(R, R_eigen);
            T_right_left_.block<3, 3>(0, 0) = R_eigen;
            T_right_left_(0, 3) = T.at<double>(0);
            T_right_left_(1, 3) = T.at<double>(1);
            T_right_left_(2, 3) = T.at<double>(2);

            cv::Mat R1, R2, P1, P2,Q;
            cv::stereoRectify(cvK_left, cvD_left, cvK_right, cvD_right,cv::Size(width, height), R, T,
                              cvRectMat_left, cvRectMat_right, cvP_left, cvP_right, Q);

            std::vector<double> vD_left, vK_left, vRectMat_left, vP_left;
            std::vector<double> vD_right, vK_right, vRectMat_right, vP_right;

            vD_left.assign((double*) cvD_left.data, (double*) cvD_left.data + cvD_left.total());
            vK_left.assign((double*) cvK_left.data, (double*) cvK_left.data + cvK_left.total());
            vD_right.assign((double*) cvD_right.data, (double*) cvD_right.data + cvD_right.total());
            vK_right.assign((double*) cvK_right.data, (double*) cvK_right.data + cvK_right.total());

            vRectMat_left.assign((double*) cvRectMat_left.data, (double*) cvRectMat_left.data + cvRectMat_left.total());
            vP_left.assign((double*) cvP_left.data, (double*) cvP_left.data + cvP_left.total());
            vRectMat_right.assign((double*) cvRectMat_right.data, (double*) cvRectMat_right.data + cvRectMat_right.total());
            vP_right.assign((double*) cvP_right.data, (double*) cvP_right.data + cvP_right.total());

            std::string distortionModel = "plumb_bob";

            cam_left_ptr_->setIntrinsicParameters(
                    width, height,
                    leftName,
                    distortionModel,
                    vD_left, vK_left, vRectMat_left, vP_left);
            cam_right_ptr_->setIntrinsicParameters(
                    width, height,
                    rightName,
                    distortionModel,
                    vD_right, vK_right, vRectMat_right, vP_right);

            if (bPrintCalibInfo)
                printCalibInfo();
        }

        void CameraSystem::loadCalibInfo(const std::string &cameraSystemDir, bool bPrintCalibInfo) {
            const std::string left_cam_calib_dir(cameraSystemDir + "/left.yaml");
            const std::string right_cam_calib_dir(cameraSystemDir + "/right.yaml");
            YAML::Node leftCamCalibInfo = YAML::LoadFile(left_cam_calib_dir);
            YAML::Node rightCamCalibInfo = YAML::LoadFile(right_cam_calib_dir);

            // load calib (left)
            size_t width = leftCamCalibInfo["image_width"].as<int>();
            size_t height = leftCamCalibInfo["image_height"].as<int>();
            std::string cameraNameLeft = leftCamCalibInfo["camera_name"].as<std::string>();
            std::string cameraNameRight = rightCamCalibInfo["camera_name"].as<std::string>();
            std::string distortion_model = leftCamCalibInfo["distortion_model"].as<std::string>();
            std::vector<double> vD_left, vK_left, vRectMat_left, vP_left;
            std::vector<double> vD_right, vK_right, vRectMat_right, vP_right;
            std::vector<double> vT_right_left;

            vD_left = leftCamCalibInfo["distortion_coefficients"]["data"].as<std::vector<double> >();
            vK_left = leftCamCalibInfo["camera_matrix"]["data"].as<std::vector<double> >();
            vRectMat_left = leftCamCalibInfo["rectification_matrix"]["data"].as<std::vector<double> >();
            vP_left = leftCamCalibInfo["projection_matrix"]["data"].as<std::vector<double> >();

            vD_right = rightCamCalibInfo["distortion_coefficients"]["data"].as<std::vector<double> >();
            vK_right = rightCamCalibInfo["camera_matrix"]["data"].as<std::vector<double> >();
            vRectMat_right = rightCamCalibInfo["rectification_matrix"]["data"].as<std::vector<double> >();
            vP_right = rightCamCalibInfo["projection_matrix"]["data"].as<std::vector<double> >();

            vT_right_left = leftCamCalibInfo["T_right_left"]["data"].as<std::vector<double> >();

            cam_left_ptr_->setIntrinsicParameters(
                    width, height,
                    cameraNameLeft,
                    distortion_model,
                    vD_left, vK_left, vRectMat_left, vP_left);
            cam_right_ptr_->setIntrinsicParameters(
                    width, height,
                    cameraNameRight,
                    distortion_model,
                    vD_right, vK_right, vRectMat_right, vP_right);

            T_right_left_ = Eigen::Matrix<double, 3, 4, Eigen::RowMajor>(vT_right_left.data());

            if (bPrintCalibInfo)
                printCalibInfo();
        }

        void CameraSystem::printCalibInfo() {
            LOG(INFO) << "============================================" << std::endl;
            LOG(INFO) << "Left Camera" << std::endl;
            LOG(INFO) << "--name:" << cam_left_ptr_->cameraName_;
            LOG(INFO) << "--image_width: " << cam_left_ptr_->width_;
            LOG(INFO) << "--image_height: " << cam_left_ptr_->height_;
            LOG(INFO) << "--distortion model: " << cam_left_ptr_->distortion_model_;
            LOG(INFO) << "--distortion_coefficients:\n" << cam_left_ptr_->D_;
            LOG(INFO) << "--rectification_matrix:\n" << cam_left_ptr_->RectMat_;
            LOG(INFO) << "--projection_matrix:\n" << cam_left_ptr_->P_;
            LOG(INFO) << "--T_right_left:\n" << T_right_left_;

            LOG(INFO) << "============================================" << std::endl;
            LOG(INFO) << "Right Camera:" << std::endl;
            LOG(INFO) << "--name:" << cam_right_ptr_->cameraName_;
            LOG(INFO) << "--image_width: " << cam_right_ptr_->width_;
            LOG(INFO) << "--image_height: " << cam_right_ptr_->height_;
            LOG(INFO) << "--distortion model:" << cam_right_ptr_->distortion_model_;
            LOG(INFO) << "--distortion_coefficients:\n" << cam_right_ptr_->D_;
            LOG(INFO) << "--rectification_matrix:\n" << cam_right_ptr_->RectMat_;
            LOG(INFO) << "--projection_matrix:\n" << cam_right_ptr_->P_;
            LOG(INFO) << "============================================" << std::endl;
        }

        bool PerspectiveCamera::boundaryCheck(size_t x, size_t y) const{
            return 0 <= x && x < width_ && 0 <= y && y < height_;
        }

    } // container

} //esvo_core