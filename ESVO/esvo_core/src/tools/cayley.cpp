#include <esvo_core/tools/cayley.h>

Eigen::Matrix3f
esvo_core::tools::cayley2rot(const Eigen::Vector3f &cayley) {
    Eigen::Matrix3f R;
    float c0 = cayley[0];
    float c1 = cayley[1];
    float c2 = cayley[2];
    float c0_sq = c0 * c0;
    float c1_sq = c1 * c1;
    float c2_sq = c2 * c2;
    float scale = 1.f + c0_sq + c1_sq + c2_sq;


    R(0, 0) = 1.f + c0_sq - c1_sq - c2_sq;
    R(0, 1) = 2 * (c0 * c1 - c2);
    R(0, 2) = 2 * (c0 * c2 + c1);
    R(1, 0) = 2 * (c0 * c1 + c2);
    R(1, 1) = 1.f - c0_sq + c1_sq - c2_sq;
    R(1, 2) = 2 * (c1 * c2 - c0);
    R(2, 0) = 2 * (c0 * c2 - c1);
    R(2, 1) = 2 * (c1 * c2 + c0);
    R(2, 2) = 1.f - c0_sq - c1_sq + c2_sq;

    R = (1.f / scale) * R;
    return R;
}

Eigen::Vector3d
esvo_core::tools::rot2cayley(const Eigen::Matrix3d &R) {
    Eigen::Matrix3d C1;
    Eigen::Matrix3d C2;
    Eigen::Matrix3d C;
    C1 = R - Eigen::Matrix3d::Identity();
    C2 = R + Eigen::Matrix3d::Identity();
    C = C1 * C2.inverse();

    Eigen::Vector3d cayley;
    cayley[0] = -C(1, 2);
    cayley[1] = C(0, 2);
    cayley[2] = -C(0, 1);

    return cayley;
}
