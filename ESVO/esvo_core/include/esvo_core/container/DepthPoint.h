#ifndef ESVO_CORE_CONTAINER_DEPTHPOINT_H
#define ESVO_CORE_CONTAINER_DEPTHPOINT_H

#include <memory>
#include <cstdlib>
#include <Eigen/Eigen>
#include <ostream>
#include <kindr/minimal/quat-transformation.h>


namespace esvo_core {
    namespace container {

        using Transformation = kindr::minimal::QuatTransformation;

        class DepthPoint {
        public:
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
            typedef std::shared_ptr<DepthPoint> Ptr;

            DepthPoint();

            DepthPoint(int row, int col);

            virtual ~DepthPoint();

            int row() const;

            int col() const;

            void update_x(const Eigen::Vector2d &x);

            const Eigen::Vector2d &x() const;

            double &invDepth();

            const double &invDepth() const;

            double &scaleSquared();

            const double &scaleSquared() const;

            double &nu();

            const double &nu() const;

            double &variance();

            const double &variance() const;

            double &residual();

            const double &residual() const;

            size_t &age();

            const size_t &age() const;

            void boundVariance();

            void update(double invDepth, double variance);// Gaussian distribution
            void update_studentT(double invDepth, double scale2, double variance, double nu); // T distribution

            void update_p_cam(const Eigen::Vector3d &p);

            [[nodiscard]] const Eigen::Vector3d &p_cam() const;

            void updatePose(const Transformation &T_world_cam);// used in the fusion of each newly estimate.
            // Therefore, it is not necessary to call updatePose for those created in the fusion. Because those share
            // the pose of the fused depthFrame.

            [[nodiscard]] const Transformation &T_world_cam() const;

            [[nodiscard]] bool valid() const;

            [[nodiscard]] bool valid(double var_threshold,
                       size_t age_threshold,
                       double invDepth_max,
                       double invDepth_min) const;

            //copy an element without the location
            void copy(const DepthPoint &copy);

        private:
            //coordinates in the image
            int row_;
            int col_;
            Eigen::Vector2d x_;

            //inverse depth parameters
            double invDepth_ = -1.;
            double scaleSquared_ = 0.;// squared scale
            double nu_ = 0.;
            double variance_ = 0.;
            double residual_ = 0.;

            // count the number of fusion has been applied on a depth point
            size_t age_;

            //3D point (updated in reference frame before tracking)
            Eigen::Vector3d p_cam_;
            Transformation T_world_cam_;
        public:
            friend std::ostream &operator<<(std::ostream &os, const DepthPoint &point);
        };
    }
}

#endif //ESVO_CORE_CONTAINER_DEPTHPOINT_H