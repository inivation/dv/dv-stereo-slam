#ifndef ESVO_CORE_CONTAINER_TIMESURFACEOBSERVATION_H
#define ESVO_CORE_CONTAINER_TIMESURFACEOBSERVATION_H

#include <kindr/minimal/quat-transformation.h>

#include <opencv2/core/eigen.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include <esvo_core/tools/TicToc.h>
#include <esvo_core/tools/utils.h>

//#define TIME_SURFACE_OBSERVATION_LOG
namespace esvo_core
{
    using namespace tools;
    namespace container
    {
        struct TimeSurfaceObservation
        {
            TimeSurfaceObservation(
                    cv::Mat left,
                    cv::Mat right,
                    Transformation &tr,
                    size_t id,
                    bool bCalcSaeGradient = false)
                    : tr_(tr),
                      id_(id)
            {
                cvImagePtr_left_ = left;
                cvImagePtr_right_ = right;

                if (bCalcSaeGradient)
                {
#ifdef TIME_SURFACE_OBSERVATION_LOG
                    TicToc tt;
      tt.tic();
#endif
                    cv::Mat cv_dSAE_du_left, cv_dSAE_dv_left;
                    cv::Sobel(left, cv_dSAE_du_left, CV_64F, 1, 0);
                    cv::Sobel(left, cv_dSAE_dv_left, CV_64F, 0, 1);
                    cv::cv2eigen(cv_dSAE_du_left, dTS_du_left_);
                    cv::cv2eigen(cv_dSAE_dv_left, dTS_dv_left_);
#ifdef TIME_SURFACE_OBSERVATION_LOG
                    LOG(INFO) << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@Sobel computation (" << id_ << ") takes " << tt.toc() << " ms.";
#endif
                }
            }

            TimeSurfaceObservation(
                    cv::Mat left,
                    size_t id)
                    : id_(id) {
                cvImagePtr_left_ = left;
            }

            // override version without initializing the transformation in the constructor.
            TimeSurfaceObservation(
                    cv::Mat left,
                    cv::Mat right,
                    size_t id,
                    bool bCalcSaeGradient = false)
                    : id_(id)
            {
                cvImagePtr_left_ = left;
                cvImagePtr_right_ = right;

                if (bCalcSaeGradient)
                {
#ifdef TIME_SURFACE_OBSERVATION_LOG
                    TicToc tt;
      tt.tic();
#endif
                    cv::Mat cv_dSAE_du_left, cv_dSAE_dv_left;
                    cv::Mat cv_dSAE_du_right, cv_dSAE_dv_right;
                    cv::Sobel(left, cv_dSAE_du_left, CV_64F, 1, 0);
                    cv::Sobel(left, cv_dSAE_dv_left, CV_64F, 0, 1);

                    cv::cv2eigen(cv_dSAE_du_left, dTS_du_left_);
                    cv::cv2eigen(cv_dSAE_dv_left, dTS_dv_left_);

#ifdef TIME_SURFACE_OBSERVATION_LOG
                    LOG(INFO) << "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@Sobel computation (" << id_ << ") takes " << tt.toc() << " ms.";
#endif
                }
            }

            void convert2eigen() {
                cv::cv2eigen(cvImagePtr_left_, TS_left_);
                cv::cv2eigen(cvImagePtr_right_, TS_right_);
            }

            TimeSurfaceObservation()
            {};

            inline bool isEmpty()
            {
                return cvImagePtr_left_.empty() || cvImagePtr_right_.empty();
            }

            inline void setTransformation(Transformation &tr)
            {
                tr_ = tr;
            }

            inline void GaussianBlurTS(size_t kernelSize)
            {
//                cv::Mat mat_left_, mat_right_;
                cv::GaussianBlur(cvImagePtr_left_, cvImagePtr_left_,
                                 cv::Size(kernelSize, kernelSize), 0.0);
                cv::GaussianBlur(cvImagePtr_right_, cvImagePtr_right_,
                                 cv::Size(kernelSize, kernelSize), 0.0);
                cv::cv2eigen(cvImagePtr_left_, TS_left_);
                cv::cv2eigen(cvImagePtr_right_, TS_right_);
            }

            inline void getTimeSurfaceNegative(size_t kernelSize, double contrast = 1.0)
            {
                if (kernelSize > 0)
                {
                    cv::Mat mat_left_;
                    if (contrast != 1.0) {
                        cv::GaussianBlur(cvImagePtr_left_ * contrast, mat_left_,
                                         cv::Size(kernelSize, kernelSize), 0.0);
                    } else {
                        cv::GaussianBlur(cvImagePtr_left_, mat_left_,
                                         cv::Size(kernelSize, kernelSize), 0.0);
                    }
                    cv_sae_flipped_left = 255 - mat_left_;
                }
                else
                {
                    cv_sae_flipped_left = 255 - cvImagePtr_left_;
                }
            }

            inline void computeTsNegativeGrad()
            {
                cv::Sobel(cv_sae_flipped_left, cv_dFlippedSAE_du_left, CV_16S, 1, 0);
                cv::Sobel(cv_sae_flipped_left, cv_dFlippedSAE_dv_left, CV_16S, 0, 1);
            }

            cv::Mat cv_sae_flipped_left;
            Eigen::MatrixXd TS_left_, TS_right_;
            cv::Mat cvImagePtr_left_, cvImagePtr_right_;
            Transformation tr_;
            Eigen::MatrixXd dTS_du_left_, dTS_dv_left_;
            cv::Mat cv_dFlippedSAE_du_left, cv_dFlippedSAE_dv_left;
            size_t id_;
        };

        using TimeSurfaceHistory = std::map<int64_t , TimeSurfaceObservation>;
        using StampedTimeSurfaceObs = std::pair<int64_t, TimeSurfaceObservation>;

        inline static TimeSurfaceHistory::iterator TSHistory_lower_bound(TimeSurfaceHistory &ts_history, int64_t t)
        {
            return std::lower_bound(ts_history.begin(), ts_history.end(), t,
                                    [](const std::pair<int64_t, TimeSurfaceObservation> &tso, int64_t t) {
                                        return tso.first < t;
                                    });
        }

        inline static TimeSurfaceHistory::iterator TSHistory_upper_bound(TimeSurfaceHistory &ts_history, int64_t t)
        {
            return std::upper_bound(ts_history.begin(), ts_history.end(), t,
                                    [](const int64_t &t, const std::pair<int64_t, TimeSurfaceObservation> &tso) {
                                        return t < tso.first;
                                    });
        }
    }
}

#endif //ESVO_CORE_CONTAINER_TIMESURFACEOBSERVATION_H