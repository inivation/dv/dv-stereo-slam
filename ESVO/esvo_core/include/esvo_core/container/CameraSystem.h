#ifndef ESVO_CORE_CONTAINER_CAMERASYSTEM_H
#define ESVO_CORE_CONTAINER_CAMERASYSTEM_H

#include <string>
#include <Eigen/Eigen>
#include <opencv2/opencv.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/next_prior.hpp>
#include <yaml-cpp/yaml.h>

namespace esvo_core
{
namespace container
{
class PerspectiveCamera
{
  public:
  PerspectiveCamera();
  virtual ~PerspectiveCamera();
  using Ptr = std::shared_ptr<PerspectiveCamera>;

  void setIntrinsicParameters(
    size_t width, size_t height,
    std::string& cameraName,
    std::string& distortion_model,
    std::vector<double>& vD,
    std::vector<double>& vK,
    std::vector<double>& vRectMat,
    std::vector<double>& vP);

  void preComputeRectifiedCoordinate();

  //Eigen::Matrix<double, 2, 1> getRectifiedUndistortedCoordinate(int xcoor, int ycoor);

  const cv::Point2i& getRectifiedUndistortedCoordinate(uint32_t xcoor, uint32_t ycoor) const;

  void rectifyImage(const cv::Mat& input, cv::Mat& output);

  void cam2World(const Eigen::Vector2d &x, double invDepth, Eigen::Vector3d &p) const;

  void world2Cam(const Eigen::Vector3d &p, Eigen::Vector2d &x);

  void world2Cam(const Eigen::Vector3f &p, Eigen::Vector2f&x);

  bool boundaryCheck(size_t x, size_t y) const;


public:
  size_t width_ = 0, height_ = 0;
  std::string cameraName_;
  std::string distortion_model_;
  Eigen::Matrix<double, 4, 1> D_;
  Eigen::Matrix3d K_;
  Eigen::Matrix3d RectMat_;
  Eigen::Matrix<double, 3, 4> P_;
  Eigen::Matrix<float, 3, 3> R_f;
  Eigen::Matrix<float, 3, 1> T_f;
  Eigen::Matrix<double, 3, 3> R_d;
  Eigen::Matrix<double, 3, 1> T_d;
  Eigen::Matrix<double, 4, 4> P_tilde;

  std::vector<cv::Point2i> precomputed_rectified_points_;
  cv::Mat undistort_map1_, undistort_map2_;
  Eigen::MatrixXi UndistortRectify_mask_;

    bool testUndistortionMask(const cv::Point2i& p) const;
};

class   CameraSystem
{
  public:
  CameraSystem(const std::string& calibInfoDir, bool bPrintCalibInfo = false);
  virtual ~CameraSystem();
  using Ptr = std::shared_ptr<CameraSystem>;

  void computeBaseline();
  void loadCalibInfo(const std::string & cameraSystemDir, bool bPrintCalibInfo = false);
  void printCalibInfo();

  PerspectiveCamera::Ptr cam_left_ptr_, cam_right_ptr_; // intrinsics
  Eigen::Matrix<double, 3, 4> T_right_left_;// extrinsics
  double baseline_;

  void loadCalibInfoXML(const std::string &cameraSystemDir, bool bPrintCalibInfo);
};
}
}

#endif //ESVO_CORE_CONTAINER_CAMERASYSTEM_H