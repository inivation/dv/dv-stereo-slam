#ifndef ESVO_CORE_CORE_EVENTMATCHPAIR_H
#define ESVO_CORE_CORE_EVENTMATCHPAIR_H

#include <utility>
#include <vector>
#include <esvo_core/tools/utils.h>
#include <esvo_core/container/CameraSystem.h>
#include <esvo_core/container/TimeSurfaceObservation.h>
#include <deque>

namespace esvo_core {
    using namespace container;
    using namespace tools;
    namespace core {
        struct EventMatchPair {
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW

            EventMatchPair() = default;

            EventMatchPair(int16_t x, int16_t y, int64_t timestamp, double invDepth, const Transformation &tf) :
                    x_left_(static_cast<double>(x), static_cast<double>(y)), t_(timestamp), invDepth_(invDepth) {
                trans_ = tf;
            }

            EventMatchPair(const cv::Point2i &pos, int64_t timestamp, double invDepth, const Transformation& tf) :
                    t_(timestamp), invDepth_(invDepth) {
                x_left_ = Eigen::Vector2d(static_cast<double>(pos.x), static_cast<double>(pos.y));
                trans_ = tf;
            }

            EventMatchPair(Eigen::Vector2d pos, int64_t timestamp, double invDepth, const Transformation& tf) :
                    x_left_(std::move(pos)), t_(timestamp), invDepth_(invDepth) {
                trans_ = tf;
            }

            // rectified_event coordinate (left, right)
            Eigen::Vector2d x_left_;
            // timestamp
            int64_t t_ = 0LL;
            // inverse depth
            double invDepth_ = 0.0;
            // pose of virtual view (T_world_virtual)
            Transformation trans_;
        };
    }
}

#endif //ESVO_CORE_CORE_EVENTMATCHPAIR_H
