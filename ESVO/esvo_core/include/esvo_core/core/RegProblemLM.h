#ifndef ESVO_CORE_CORE_REGPROBLEMLM_H
#define ESVO_CORE_CORE_REGPROBLEMLM_H

#include <esvo_core/container/CameraSystem.h>
#include <esvo_core/container/ResidualItem.h>
#include <esvo_core/optimization/OptimizationFunctor.h>
#include <esvo_core/container/TimeSurfaceObservation.h>
#include <esvo_core/tools/utils.h>

#include <pcl/point_types.h>

namespace esvo_core
{
    using namespace container;
    using namespace tools;
    namespace core
    {
        struct RegProblemConfig
        {
            enum LSNorm {
                Huber, l2
            };

            using Ptr = std::shared_ptr<RegProblemConfig>;
            RegProblemConfig(
                    size_t patchSize_X,
                    size_t patchSize_Y,
                    size_t kernelSize,
                    const std::string &LSnorm__,
                    double huber_threshold,
                    double invDepth_min_range = 0.2,
                    double invDepth_max_range = 2.0,
                    double contrast = 1.0,
                    const size_t MIN_NUM_EVENTS = 1000,
                    const size_t MAX_REGISTRATION_POINTS = 500,
                    const size_t BATCH_SIZE = 200,
                    const size_t MAX_ITERATION = 10):
                    patchSize_X_(patchSize_X),
                    patchSize_Y_(patchSize_Y),
                    kernelSize_(kernelSize),
                    huber_threshold_(huber_threshold),
                    invDepth_min_range_(invDepth_min_range),
                    invDepth_max_range_(invDepth_max_range),
                    contrast_(contrast),
                    MIN_NUM_EVENTS_(MIN_NUM_EVENTS),
                    MAX_REGISTRATION_POINTS_(MAX_REGISTRATION_POINTS),
                    BATCH_SIZE_(BATCH_SIZE),
                    MAX_ITERATION_(MAX_ITERATION)
            {
                if (LSnorm__ == "Huber") {
                    LSnorm_ = LSNorm::Huber;
                } else {
                    LSnorm_ = LSNorm::l2;
                }
            }

            size_t patchSize_X_, patchSize_Y_;
            size_t kernelSize_;
            LSNorm LSnorm_ = LSNorm::Huber;
            float huber_threshold_;
            double invDepth_min_range_;
            double invDepth_max_range_;
            double contrast_ = 1.0;
            size_t MIN_NUM_EVENTS_;
            size_t MAX_REGISTRATION_POINTS_;
            size_t BATCH_SIZE_;
            size_t MAX_ITERATION_;
        };

        struct RefFrame
        {
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
            int64_t t_ = 0LL;
            PointCloud vPointXYZPtr_;
            Transformation tr_;
        };

        struct CurFrame
        {
            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
            int64_t t_ = 0LL;
            TimeSurfaceObservation* pTsObs_;
            Transformation tr_;
//            Transformation tr_old_;
            size_t numEventsSinceLastObs_;
        };

        struct RegProblemLM : public optimization::OptimizationFunctor<float>
        {
            struct Job
            {
                EIGEN_MAKE_ALIGNED_OPERATOR_NEW
                ResidualItems* pvRI_;
                const TimeSurfaceObservation* pTsObs_;
                const Eigen::Matrix4f* T_left_ref_;
                size_t i_thread_;
            };

            EIGEN_MAKE_ALIGNED_OPERATOR_NEW
            RegProblemLM(
                    const CameraSystem::Ptr& camSysPtr,
                    const RegProblemConfig::Ptr& rpConfig_ptr,
                    size_t numThread = 1);
            void setProblem(RefFrame* ref, CurFrame* cur, bool bComputeGrad = false);
            void setStochasticSampling(size_t offset, size_t N);

            void getWarpingTransformation(
                    Eigen::Matrix4f& warpingTransf,
                    const Eigen::Matrix<float, 6, 1>& x) const;
            void addMotionUpdate( const Eigen::Matrix<float, 6, 1>& dx );
            void setPose();
            Eigen::Matrix4d getPose();

            // optimization
            int operator()( const Eigen::Matrix<float,6,1>& x, Eigen::VectorXf& fvec ) const;
            void thread( Job& job ) const;
            int df( const Eigen::Matrix<float,6,1>& x, Eigen::MatrixXf& fjac ) const;
            void computeJ_G(const Eigen::Matrix<float,6,1>&x, Eigen::Matrix<float,12,6>& J_G);

            // utils
            bool isValidPatch(
                    Eigen::Vector2f& patchCentreCoord,
                    Eigen::MatrixXi& mask,
                    size_t wx,
                    size_t wy) const;

            bool reprojection(
                    const Eigen::Vector3f& p,
                    const Eigen::Matrix4f& warpingTransf,
                    Eigen::Vector2f &x1_s) const;

            bool reprojection(
                    const Eigen::Vector3f& p_left,
                    Eigen::Vector2f &x1_s) const;

            bool isValidPatch1x1(
                    Eigen::Vector2f& patchCentreCoord,
                    Eigen::MatrixXi& mask) const;

  bool patchInterpolation(
    const cv::Mat &img,
    const Eigen::Vector2f &location,
    Eigen::MatrixXf &patch,
    bool debug = false) const;

    static float patchInterpolation1x1(
            const cv::Mat &img,
            const Eigen::Vector2f &location) ;

    static bool patchInterpolation1x1Sobel(
            const cv::Mat &sobelX,
            const cv::Mat &sobelY,
            const Eigen::Vector2f &location,
            float &gx,
            float &gy
    );
  //
  CameraSystem::Ptr camSysPtr_;
  RegProblemConfig::Ptr rpConfigPtr_;
  size_t patchSize_;

            size_t NUM_THREAD_;
            size_t numPoints_;
            size_t numBatches_;

            ResidualItems ResItems_, ResItemsStochSampled_;
            TimeSurfaceObservation* pTsObs_;
            RefFrame* ref_;
            CurFrame* cur_;

            Eigen::Matrix<float,4,4> T_world_left_;// to record the current pose
            Eigen::Matrix<float,4,4> T_world_ref_;// to record the ref pose (local ref map)
            Eigen::Matrix3f R_;//R_ref_cur_;
            Eigen::Vector3f t_;//t_ref_cur

            // Jacobian Constant
            Eigen::Matrix<float,12,6> J_G_0_;
            // debug
            bool bPrint_;
        };// struct RegProblemLM
    }// namespace core
}// namespace esvo_core

#endif //ESVO_CORE_CORE_REGPROBLEMLM_H