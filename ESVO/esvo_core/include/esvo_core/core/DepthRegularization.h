#ifndef ESVO_CORE_CORE_DEPTHREGULARIZATION_H
#define ESVO_CORE_CORE_DEPTHREGULARIZATION_H

#include <esvo_core/container/DepthMap.h>
#include <esvo_core/core/DepthProblem.h>
#include <memory>
namespace esvo_core
{
using namespace container;
namespace core
{
class DepthRegularization
{
public:
  typedef std::shared_ptr<DepthRegularization> Ptr;

  DepthRegularization(DepthProblemConfig::LS_Norm norm);
  virtual ~DepthRegularization();

  void apply( DepthMap::Ptr & depthMapPtr );

private:
  DepthProblemConfig::LS_Norm LS_norm_;
  size_t _regularizationRadius;
  size_t _regularizationMinNeighbours;
  size_t _regularizationMinCloseNeighbours;
};
}// core
}// esvo_core

#endif //ESVO_CORE_CORE_DEPTHREGULARIZATION_H