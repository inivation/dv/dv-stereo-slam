import cv2
import numpy as np
import argparse
import os


class Calibration:

    def __init__(self, name, file_node):
        self.name = name
        self.width = int(file_node.getNode("image_width").real())
        self.height = int(file_node.getNode("image_height").real())
        self.camera_matrix = file_node.getNode("camera_matrix").mat()
        self.distortion = file_node.getNode("distortion_coefficients").mat()
        if self.distortion[4] == 0.:
            self.distortion = self.distortion[0:4]
        elif self.distortion[5] == 0.:
            self.distortion = self.distortion[0:5]

    reprojection_matrix = np.array([])
    rectification_matrix = np.array([])

    def write_to_file(self, filename, R_T):
        filestorage = cv2.FileStorage(filename, cv2.FILE_STORAGE_WRITE)
        filestorage.write("image_width", str(self.width))
        filestorage.write("image_height", str(self.height))
        filestorage.write("camera_name", self.name)
        filestorage.write("camera_matrix", self.camera_matrix)
        filestorage.write("distortion_model", "plumb_bob")
        filestorage.write("distortion_coefficients", self.distortion)
        filestorage.write("rectification_matrix", self.rectification_matrix)
        filestorage.write("projection_matrix", self.reprojection_matrix)
        filestorage.write("T_right_left", R_T)
        filestorage.release()


class StereoCalibration:
    def __init__(self, xml_file, left_name, right_name):
        contents = cv2.FileStorage(xml_file, cv2.FILE_STORAGE_READ)
        left_calibration = contents.getNode(left_name)
        right_calibration = contents.getNode(right_name)
        self.left = Calibration(left_name, left_calibration)
        self.right = Calibration(right_name, right_calibration)

        self.R = contents.getNode("R").mat()
        self.T = contents.getNode("T").mat()

    def calculate_rectification(self):
        result = cv2.stereoRectify(self.left.camera_matrix, self.left.distortion, self.right.camera_matrix,
                                   self.right.distortion, (self.left.width, self.left.height),
                                   self.R, self.T)
        self.left.rectification_matrix = result[0]
        self.right.rectification_matrix = result[1]
        self.left.reprojection_matrix = result[2]
        self.right.reprojection_matrix = result[3]

    def write_yaml(self, output_directory):
        try:
            os.makedirs(output_directory)
        except OSError:
            pass
        R_T = np.concatenate((self.R, self.T), axis=1)
        self.left.write_to_file(output_directory + "/left.yaml", R_T)
        self.right.write_to_file(output_directory + "/right.yaml", R_T)


def main():
    parser = argparse.ArgumentParser(description='Import DV calibration.')
    parser.add_argument('--calibration', '-c', dest='calibration', type=str, required=True,
                        help='Calibration XML file.')
    parser.add_argument('--output-dir', '-o', dest='output', type=str, required=True,
                        help='Output directory to place generated files.')
    parser.add_argument('--left-name', '-l', dest='left_name', type=str, required=True,
                        help='Left camera name.')
    parser.add_argument('--right-name', '-r', dest='right_name', type=str, required=True,
                        help='Right camera name.')

    args = parser.parse_args()

    stereo_calibration = StereoCalibration(args.calibration, args.left_name, args.right_name)
    stereo_calibration.calculate_rectification()
    stereo_calibration.write_yaml(args.output)


if __name__ == '__main__':
    main()
