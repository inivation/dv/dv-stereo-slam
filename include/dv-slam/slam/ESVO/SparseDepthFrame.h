//
// Created by rokas on 2020-12-23.
//

#pragma once

#include <opencv2/opencv.hpp>
#include <memory>

#include <esvo_core/container/EventMatchPair.h>
#include <esvo_core/container/DepthPoint.h>
#include <esvo_core/tools/utils.h>

#include <dv-sdk/processing/core.hpp>

namespace slam {
    typedef std::shared_ptr<std::vector<esvo_core::container::DepthPoint>> DepthPointsPtr;
    typedef std::shared_ptr<std::vector<esvo_core::core::EventMatchPair>> DepthEventsPtr;

    class SparseDepthFrame {
    public:
        cv::Mat disparity;
        DepthPointsPtr depthPoints = nullptr;
        int64_t timestamp = 0LL;
        dv::EventStore events;
        DepthEventsPtr depthEvents;
        esvo_core::tools::Transformation transform;
    };

    typedef std::shared_ptr<SparseDepthFrame> SparseDepthFramePtr;

}
