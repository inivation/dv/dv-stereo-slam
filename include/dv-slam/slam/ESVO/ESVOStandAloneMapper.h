//
// Created by rokas on 2020-10-05.
//

#pragma once

#include <esvo_core/container/CameraSystem.h>
#include <esvo_core/container/DepthMap.h>
#include <esvo_core/core/DepthProblem.h>
#include <esvo_core/core/DepthProblemSolver.h>
#include <esvo_core/core/EventBM.h>
#include <esvo_core/tools/utils.h>
#include <esvo_core/tools/Visualization.h>

//#include <esvo_core/DVS_MappingStereoConfig.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/ximgproc.hpp>

#include <map>
#include <deque>
#include <mutex>
#include <future>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#ifdef WITH_OCTOMAP
#include <octomap/octomap.h>
#endif

#include <boost/optional.hpp>
#include <boost/lockfree/spsc_queue.hpp>

#include <dv-slam/utilities/Transformer.h>
#include <dv-slam/utilities/SafeEventStore.h>
#include <dv-slam/slam/ESVO/SparseDepthFrame.h>

#if defined USE_CUDA_STEREO
#define CUDA_STEREO_BM 1
#else
#define CUDA_STEREO_BM 0
#endif

#if CUDA_STEREO_BM
#include <libsgm.h>
#include <libsgm_wrapper.h>
#include <cuda_runtime.h>

struct device_buffer {
    device_buffer() = default;

    explicit device_buffer(size_t count) {
        allocate(count);
    }

    void allocate(size_t count) {
        cudaMalloc(&data, count);
        dataCPU = malloc(count);
        size = count;
    }

    ~device_buffer() {
        if (data != nullptr) {
            cudaFree(data);
        }
    }

    void upload(const cv::Mat& input) {
        size_t inputSize = input.rows * input.step;
        if (size == 0) {
            allocate(inputSize);
        } else if (size != inputSize) {
            throw runtime_error("Input preallocation is invalid");
        }
        cudaMemcpy(data, input.ptr<uint8_t>(), inputSize, cudaMemcpyHostToDevice);
    }

    cv::Mat download(const cv::Size& outputSize, int type, size_t step) const {
        cudaMemcpy(data, dataCPU, size, cudaMemcpyDeviceToHost);
        return cv::Mat(outputSize, type, dataCPU, step);
    }

    size_t size = 0;
    void *data = nullptr;
    void *dataCPU = nullptr;
};

#endif

using namespace esvo_core;
using namespace esvo_core::core;
using namespace slam;

class ESVOStandAloneMapper
{
    enum SystemStatus
    {
        INITIALIZATION,
        SYSTEM_WORKING
    };

    struct EdgeInfo {
        double invDepth;
        int64_t timestamp;

        EdgeInfo(double invDepth_, int64_t timestamp_) : invDepth(invDepth_), timestamp(timestamp_) {}
    };
    typedef boost::lockfree::spsc_queue<std::pair<int64_t, std::shared_ptr<Transformation>>> TransformationQueue;

public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    explicit ESVOStandAloneMapper(std::string  calibration);
    void pushEvents(const std::shared_ptr<const dv::EventPacket>& eventsPtr);
    void pushFramePair(cv::Mat &left, cv::Mat &right, int64_t t_new_TS);
    void pushTransformation(const Transformation& tf, int64_t t_new_TS);

    // mapping
    SparseDepthFramePtr EBM_MappingAtTime(StampedTimeSurfaceObs& TS_obs_);
    SparseDepthFramePtr MappingAtTime(StampedTimeSurfaceObs& TS_obs_);
    SparseDepthFramePtr InitializationAtTime(StampedTimeSurfaceObs& TS_obs_);
    boost::optional<StampedTimeSurfaceObs> dataTransferring(int64_t timestamp);
    std::string calibInfoDir_;
    CameraSystem::Ptr camSysPtr_;
    DepthProblemConfig::Ptr dpConfigPtr_;
    SparseDepthFramePtr SGM_MappingAtTime(StampedTimeSurfaceObs& TS_obs_);
    TransformationQueue transformQueue = TransformationQueue(100);


    static cv::Mat getDepthFrameImage(const cv::Mat& background, const DepthFrame::Ptr& map);
    bool isDepthEstimationOnly() const;

    void setDepthEstimationOnly(bool depthEstimationOnly);
    bool isUseEventBlockMatcher() const;
    void setUseEventBlockMatcher(bool useEventBlockMatcher);
    void setUseHotPixels(bool useHotPixels);
    void setMinimumDepthPointsToInitialize(size_t initSgmDpNumThreshold);
    void setStdVarVisThreshold(double stdVarVisThreshold);


    void reset();
    void setEvents(const SafeEventStore::Ptr &events);
    boost::optional<StampTransformationMap> getInterpolatedTransforms(
            int64_t startTime, int64_t endTime, boost::optional<int64_t> timestep = boost::none);
    void setMinimumEventsToWork(size_t minimumEventsToWork);

private:
    DepthEventsPtr sparseDepthEstimation(StampedTimeSurfaceObs& TS_obs_, cv::Mat& dispMap);
    std::vector<std::pair<cv::Point2i, EdgeInfo> > createEdgeCoordsFromLeftImg(const StampedTimeSurfaceObs & TS_obs_,
                                                                               const cv::Mat& dispMap) const;
    // online data
    // I did not find any usage of `events_right_`
    EventQueue events_left_;
    SafeEventStore::Ptr events;
    dv::EventStore extractEvents(int64_t timestamp, int64_t thickness, size_t& eventCount);
    Transformer tf_;

    size_t TS_id_ = 0;
    TimeSurfaceHistory TS_history_;

    size_t minimum_events_to_work = 200;
    // system
    SystemStatus ESVO_System_Status_;
    std::shared_ptr<DepthProblemSolver> dpSolver_;
    Visualization visualizor_;
    EventBM ebm_;

    bool depthSmoothing = true;
public:
    void setDepthSmoothing(bool depthSmoothing);

private:

    // data transfer
    size_t totalNumCount_{};// count the number of events involved
    std::vector<dv::Event> vEventsPtr_left_SGM_;// for SGM

    bool depthEstimationOnly = false;

    bool useEventBlockMatcher = false;

    // inter-thread management
    std::mutex data_mutex_;

    /**** mapping parameters ***/
    // range and visualization threshold
    static double invDepth_min_range_;
    static double invDepth_max_range_;
    double cost_vis_threshold_;
    size_t patch_area_{};
    double residual_vis_threshold_{};
    double stdVar_vis_threshold_{};
    size_t age_max_range_{};
    size_t age_vis_threshold_{};
    bool useHotPixels = false;
    size_t INIT_SGM_DP_NUM_Threshold_ = 500;
    // module parameters
    size_t PROCESS_EVENT_NUM_ = 1000;
    size_t TS_HISTORY_LENGTH_ = 100;
    // Event Block Matching (BM) parameters
    int64_t BM_half_slice_thickness_{};
public:
    void setBmHalfSliceThickness(int64_t bmHalfSliceThickness);

    void setMinInvDepth(double minInvDepth);

    void setMaxInvDepth(double maxInvDepth);

    void updateEventBMParameters();

    void setStatusToRunning();

private:
    size_t BM_patch_size_X_{};
    size_t BM_patch_size_Y_{};
    short BM_min_disparity_ = 0;
    short BM_max_disparity_ = 128;
    short min_disparity_ = 0;
    short max_disparity_ = 128;
    size_t BM_step_{};
    double BM_ZNCC_Threshold_{};
    bool   BM_bUpDownConfiguration_{};

    // SGM parameters (Used by Initialization)
    int num_disparities_{};
    int block_size_{};
    int P1_{};
    int P2_{};
    int uniqueness_ratio_{};
#if CUDA_STEREO_BM
    std::shared_ptr<sgm::LibSGMWrapper> cuda_sgbm_ = nullptr;
#else
    cv::Ptr<cv::StereoSGBM> sgbm_;
#endif
    double disparityScale = 1.0;
    /**********************************************************/
    /******************** For test & debug ********************/
    /**********************************************************/
    // For counting the total number of fusion
    size_t TotalNumFusion_{};

    static void createEdgeMask(vector<dv::Event *> &vEventsPtr, PerspectiveCamera::Ptr &camPtr, cv::Mat &edgeMap,
                        vector<std::pair<size_t, size_t>> &vEdgeletCoordinates, bool bUndistortEvents, size_t radius);

    void createEdgeCoords(PerspectiveCamera::Ptr &camPtr,
                          vector<std::pair<size_t, size_t>> &vEdgeletCoordinates, bool bUndistortEvents);

    void createEdgeCoords(
            const dv::EventStore& sliced_events,
            size_t event_count,
            PerspectiveCamera::Ptr &camPtr,
            std::vector<std::pair<size_t, size_t> > &vEdgeletCoordinates,
            bool bUndistortEvents) const;

    std::vector<std::pair<cv::Point2i, EdgeInfo> > createEdgeCoordsMasked(
            const dv::EventStore &sliced_events, size_t event_count, PerspectiveCamera::Ptr &camPtr,
            const cv::Mat& dispMap, const StampedTimeSurfaceObs& TS_obs_,
            bool bUndistortEvents, bool compensateMotion);

    dv::EventStore extractDenoisedEvents(const dv::EventStore &vCloseEventsPtr, cv::Mat &mask, size_t maxNum);

    void createDenoisingMask(const dv::EventStore &vAllEventsPtr, cv::Mat &mask, size_t row, size_t col);
};

