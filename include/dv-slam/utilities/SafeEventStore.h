//
// Created by rokas on 2020-10-22.
//


#pragma once

#include <mutex>
#include <dv-sdk/processing/core.hpp>

namespace slam {

    class SafeEventStore : protected dv::EventStore {
    private:
        std::mutex myMutex;

        int64_t timelimit = 0LL;

    public:
        /**
         * Class for storing events with additional thread-safe and memory limiting API.
         * @param timelimit_us      Time period (in microseconds) to save the event data, older events will be discarded.
         */
        explicit SafeEventStore(int64_t timelimit_us);

        /**
         * Add events (thread-safe)
         * @param evts              Events to add
         */
        void safeAdd(const dv::EventStore& evts);

        /**
         * Add events (non thread-safe). After adding, older data that is over the time limit will be erased.
         * @param evts              Events to add
         */
        void addEvents(const dv::EventStore& evts);

        /**
         * Slice events (thread-safe)
         * @param start         Start time stamp
         * @param end           End time stamp
         * @return              Event store containing data.
         */
        [[nodiscard]] dv::EventStore safeSlice(int64_t start, int64_t end);

        /**
         * Slice events (thread-safe)
         * @param start         Start time stamp
         * @param end           End time stamp
         * @param retStart      (output) Start index in the data array
         * @param retEnd        (output) End index in the data array
         * @return              Event store containing data.
         */
        [[nodiscard]] dv::EventStore safeSlice(int64_t start, int64_t end, size_t& retStart, size_t& retEnd);

        /**
         * Slice events (nonthread-safe)
         * @param start         Start time stamp
         * @param end           End time stamp
         * @return              Event store containing data.
         */
        [[nodiscard]] dv::EventStore sliceEvents(int64_t start, int64_t end);

        /**
         * Slice events (non thread-safe)
         * @param start         Start time stamp
         * @param end           End time stamp
         * @param retStart      (output) Start index in the data array
         * @param retEnd        (output) End index in the data array
         * @return              Event store containing data.
         */
        [[nodiscard]] dv::EventStore sliceEvents(int64_t start, int64_t end, size_t& retStart, size_t& retEnd);

        [[nodiscard]] int64_t getStartTime() const;

        [[nodiscard]] int64_t getEndTime() const;

        [[nodiscard]] size_t getLength() const;

        typedef std::shared_ptr<SafeEventStore> Ptr;

    };

}
