//
// Created by rokas on 2020-10-12.
//

#pragma once

#include <iomanip>
#include <sstream>


namespace Utils {
    static std::string return_time_and_date(int64_t t) {
        time_t in_time_t = (t / 1000000LL);
        std::stringstream ss;
        ss << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X") << "." << (t % 1000000LL);
        return ss.str();
    }

}