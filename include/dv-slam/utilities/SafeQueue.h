#pragma once

#include <mutex>
#include <condition_variable>
#include <functional>
#include <thread>
#include <chrono>
#include <atomic>
#include <optional>

#include <boost/circular_buffer.hpp>
#include "CpuAffinity.h"

/**
 * Thread safe queue, that can run callbacks when new data is enqueued. Can be regarded as a very minimalistic
 * work queue.
 * @tparam T    Element of data in the queue
 */
template<class T>
class SafeQueue {
public:
    /**
     * Constructor with fixed size ring buffer
     * @param queueSize     Max size of the queue, if it is full, elements will be lost
     */
    explicit SafeQueue(size_t queueSize)
            : q(queueSize), m(), c(), runThread(nullptr), continueRun(false) {}

    /**
     * Destructor, stops asynchronous dequeue if it's running
     */
    virtual ~SafeQueue() {
        stopAutoDequeue();
    }

    /**
     * Insert a queue item to the back of the ring buffer
     * @param t     Item
     */
    void enqueue(T t) {
        std::lock_guard<std::mutex> lock(m);
        q.push_back(t);
        c.notify_one();
    }

    /**
     * Returns a queue item from the front. Blocks if queue is empty and returns when an element is inserted.
     * @return      Item to insert into the queue
     */
    T dequeue() {
        std::unique_lock<std::mutex> lock(m);
        while (q.empty()) {
            // release lock as long as the wait and reaquire it afterwards.
            c.wait(lock);
        }
        T val = q.front();
        q.pop_front();
        return val;
    }

    /**
     * Returns a queue item from the front. Blocks if queue is empty and returns when an element is inserted or a given
     * timeout is reached.
     * @param timeout       Timeout to wait for new data if queue is empty.
     * @return              An element in the front, boost::none if timeout was reached.
     */
    std::optional<T> dequeueTimeout(const std::chrono::microseconds& timeout) {
        std::unique_lock<std::mutex> lock(m);
        // release lock as long as the wait and reaquire it afterwards.
        if (q.empty() && c.wait_for(lock, timeout) == std::cv_status::timeout) {
            return {};
        }
        T val = q.front();
        q.pop_front();
        return val;
    }


    /**
     * Automatically call a callback function when an element is inserted into the queue.
     * @param callback
     * @param checkPeriod
     */
    void autoDequeueAsync(std::function<void(const T&)> callback) {
        continueRun = true;
        runThread = std::make_shared<std::thread>([&, callback] () {
            std::chrono::microseconds checkPeriod(1000);
            while (continueRun.load(std::memory_order_relaxed)) {
                if (std::optional<T> value = dequeueTimeout(checkPeriod)) {
                    try {
                        callback(value.value());
                    } catch (std::exception& exc) {
                        std::lock_guard<std::mutex> guard(exceptionMutex);
                        lastException = std::current_exception();
                        continueRun = false;
                    }
                }
            }
        });
    }

    /**
     * Stop running the auto dequeue thread
     */
    void stopAutoDequeue() {
        if (runThread && runThread->joinable()) {
            continueRun = false;
            runThread->join();
        }
    }

    /**
     * Check whether automatic dequeue thread is running.
     * @return      True if the dequeue thread is running.
     */
    bool isRunning() const {
        return continueRun.load(std::memory_order_relaxed);
    }

    /**
     * Get last exception that was caught while running auto dequeue thread.
     * @return
     */
    const exception_ptr &getLastException() {
        std::lock_guard<std::mutex> guard(exceptionMutex);
        return lastException;
    }

    /**
     * Removes any items in the queue.
     */
    void clear() {
        std::unique_lock<std::mutex> lock(m);
        q.clear();
    }

    /**
     * Lock the processing thread to run only on given cpu core.
     * @param cpuID     CPU Core id to lock the thread to.
     */
    void lockOnCpu(int cpuID) {
        // Create a cpu_set_t object representing a set of CPUs. Clear it and mark
        // only CPU i as set.
        cpu_set_t cpuset;
        CPU_ZERO(&cpuset);
        CPU_SET(cpuID, &cpuset);
        pthread_setaffinity_np(runThread->native_handle(), sizeof(cpu_set_t), &cpuset);
    }

private:
    boost::circular_buffer<T> q;
    mutable std::mutex m;
    std::mutex exceptionMutex;
    std::condition_variable c;
    std::shared_ptr<std::thread> runThread = nullptr;
    std::exception_ptr lastException = nullptr;

    // Check for execution every millisecond
    std::atomic<bool> continueRun;

};
