//
// Created by radam on 2021-02-03.
//

#pragma once


#include <kindr/minimal/quat-transformation.h>

#include <opencv2/opencv.hpp>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <chrono>

/**
 * Construct homogenous transformation matrix from rotation quaternion and translation vector.
 *
 * @param rotation
 * @param translation
 * @return
 */
Eigen::Matrix<float, 4, 4> homogenous(const Eigen::Quaternionf& rotation, const Eigen::Vector3f& translation) {
    auto transformation = Eigen::Matrix4f();
    transformation.setIdentity();
    transformation.block<3, 3>(0, 0) = rotation.toRotationMatrix();
    transformation.block<3, 1>(0, 3) = translation;
    return transformation;
}

/**
 * Inverse homogenous transformation.
 *
 * @param transformation
 * @return
 */
Eigen::Matrix<float, 4, 4> invHomogenous(const Eigen::Matrix<float, 4, 4>& transformation) {
    auto inverted = Eigen::Matrix4f();
    inverted.setIdentity();

    const auto rotation = transformation.block<3, 3>(0, 0);
    const auto translation = transformation.block<3, 1>(0, 3);

    inverted.block<3, 3>(0, 0) = rotation.transpose();
    inverted.block<3, 1>(0, 3) = -rotation.transpose() * translation;
    return inverted;
}

/**
 * Calculate the optimal grid span based on the maximum position span and the user defined density.
 *
 * @param maxSpan
 * @return
 */
int getGridSpan(const float maxSpan) {
    const int maxSpanCm = static_cast<int>(maxSpan) * 100; // [cm]

    // Available grid spans in centimeters
    const std::vector<int> gridSpans{10, 25, 50, 100, 200, 500, 1000};

    // Search for grid span which gives less than nGrids grid lines
    static constexpr int nGrids = 20;
    for (const auto& gridSpan : gridSpans) {
        if (maxSpanCm / gridSpan < nGrids) {
            return gridSpan;
        }
    }
    return gridSpans.back();
}

/**
 * Visualize the current and past poses as an image.
 */
class PoseViewer {
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    enum Mode {
        CUSTOM = 0,
        VIEW_XY,
        VIEW_YZ,
        VIEW_ZX,
        VIEW_XZ,
        VIEW_YX,
        VIEW_ZY
    };

    enum GridPlane {
        PLANE_NONE = 0,
        PLANE_XY,
        PLANE_YZ,
        PLANE_ZX
    };


    /**
     * Constructor.
     *
     * @param imageSize size of the pose visualization in pixels
     */
    explicit PoseViewer(const cv::Size2i& imageSize = cv::Size2i(640, 480)) : imageSize(imageSize),
                                                                              lastTimestamp(0) {

        lastPose = kindr::minimal::QuatTransformationTemplate<float>(Eigen::Quaternionf(1.0, 0.0, 0.0, 0.0),
                                                                     Eigen::Vector3f(0.0, 0.0, 0.0));
        initMinMax();
        updateCameraPosition(Eigen::Vector3f(0, 0, -3));
        updateCameraOrientation(0, 0, 0);
        refreshCameraMatrix();
    }

    /**
     * Update the position in which camera is located.
     *
     * @param newPosition
     */
    void updateCameraPosition(const Eigen::Vector3f& newPosition) {
        cameraPosition = newPosition;
        T_WC = homogenous(cameraOrientation, cameraPosition);
        T_CW = invHomogenous(T_WC);
    }

    /**
     * Set the mode in which the pose viewer will be working.
     *
     * @param mode
     */
    void setViewMode(const Mode mode) {
        viewMode = mode;
    }

    void setViewMode(const std::string& str) {
        static std::unordered_map<std::string, Mode> const stringToMode = {{"custom", Mode::CUSTOM},
                                                                           {"XY",     Mode::VIEW_XY},
                                                                           {"YZ",     Mode::VIEW_YZ},
                                                                           {"ZX",     Mode::VIEW_ZX},
                                                                           {"XZ",     Mode::VIEW_XZ},
                                                                           {"YX",     Mode::VIEW_YX},
                                                                           {"ZY",     Mode::VIEW_ZY},
        };
        const auto it = stringToMode.find(str);
        if (it == stringToMode.end()) {
            std::stringstream ss;
            ss << "Incorrect view mode: " << str;
            throw std::runtime_error(ss.str());
        }
        viewMode = it->second;
    }

    /**
     * Set the plane on which the grid will be displayed.
     *
     * @param plane
     */
    void setGridPlane(const GridPlane plane) {
        gridPlane = plane;
    }

    void setGridPlane(const std::string& str) {
        static std::unordered_map<std::string, GridPlane> const stringToGridPlane = {{"none", GridPlane::PLANE_NONE},
                                                                                     {"XY",   GridPlane::PLANE_XY},
                                                                                     {"YZ",   GridPlane::PLANE_YZ},
                                                                                     {"ZX",   GridPlane::PLANE_ZX}};
        const auto it = stringToGridPlane.find(str);
        if (it == stringToGridPlane.end()) {
            std::stringstream ss;
            ss << "Incorrect grid plane: " << str;
            throw std::runtime_error(ss.str());
        }
        gridPlane = it->second;
    }

    /**
     * Update the orientation of the camera expressed as XYZ Euler angles.
     *
     * @param yawDeg
     * @param pitchDeg
     * @param rollDeg
     */
    void updateCameraOrientation(const float yawDeg, const float pitchDeg, const float rollDeg) {
        cameraOrientation = Eigen::AngleAxisf(rollDeg / 180.f * M_PI, Eigen::Vector3f::UnitZ()) *
                            Eigen::AngleAxisf(pitchDeg / 180.f * M_PI, Eigen::Vector3f::UnitY()) *
                            Eigen::AngleAxisf(yawDeg / 180.f * M_PI, Eigen::Vector3f::UnitX());
        T_WC = homogenous(cameraOrientation, cameraPosition);
        T_CW = invHomogenous(T_WC);

    }

    /**
     * Update the size of output image.
     *
     * @param newSize
     */
    void updateImageSize(const cv::Size2i& newSize) {
        imageSize = newSize;

        refreshCameraMatrix();
    }

    /**
     * Update the displayed coordinate frame size.
     *
     * @param newSize [m]
     */
    void updateFrameSize(const float newSize) {
        assert(newSize > 0.f);
        frameSize = newSize;
    }

    /**
     * Update the line thickness of the drawing.
     *
     * @param newWidth [px]
     */
    void updateLineThickness(const int newThickness) {
        assert(newThickness >= 1);
        lineThickness = newThickness;
    }

    /**
     * Add a new pose to the visualization.
     *
     * @param originalPosition
     * @param orientation
     * @param timestamp [us]
     */
    void
    addPose(const Eigen::Vector3f& originalPosition, const Eigen::Quaternionf& orientation, const int64_t timestamp) {
        kindr::minimal::QuatTransformationTemplate<float> T_WB(orientation, originalPosition);
        const auto T_OW = T_WO.inverse();
        const auto T_OB = T_OW * T_WB;
        kindr::minimal::QuatTransformationTemplate<float> pose(T_OB.getRotation(), T_OB.getPosition());

        const Eigen::Vector3f position = pose.getPosition();

        path.emplace_back(Eigen::Vector4f(position.x(), position.y(), position.z(), 1.0));
        timestamps.emplace_back(timestamp);
        assert(path.size() == timestamps.size());

        // Make sure that there is always frameSize meters frame around the drawn trajectory
        if (position.x() < minPoint_W.x()) {
            minPoint_W.x() = position.x();
        }
        if (position.y() < minPoint_W.y()) {
            minPoint_W.y() = position.y();
        }
        if (position.z() < minPoint_W.z()) {
            minPoint_W.z() = position.z();
        }
        if (position.x() > maxPoint_W.x()) {
            maxPoint_W.x() = position.x();
        }
        if (position.y() > maxPoint_W.y()) {
            maxPoint_W.y() = position.y();
        }
        if (position.z() > maxPoint_W.z()) {
            maxPoint_W.z() = position.z();
        }


        // Update the bookkeping
        lastPose = kindr::minimal::QuatTransformationTemplate<float>(pose.getRotation(), pose.getPosition());
        lastTimestamp = timestamp;
    }

    /**
     * Return the timestamp of the most recent pose.
     *
     * @return
     */
    [[nodiscard]] int64_t getTimestamp() const {
        return lastTimestamp;
    }

    /**
     * Return a visualization image.
     *
     * @return
     */
    [[nodiscard]] cv::Mat getImage() {
        // Initialize variables used to draw on the image
        auto image = cv::Mat(imageSize, CV_8UC3, cv::Scalar(30, 30, 30));
        const auto pointsSpan = maxPoint_W - minPoint_W;
        auto smallerDimension = static_cast<const float>(std::min(imageSize.width, imageSize.height));
        const auto midPoint = minPoint_W + pointsSpan / 2;
        float maxSpan = 1.f;
        auto poseMask = Eigen::Vector4f(1, 1, 1, 1);

        // Set the position and orientation of the camera
        switch (viewMode) {
            case CUSTOM: {
                break;
            }
            case VIEW_XY : {
                maxSpan = std::max(pointsSpan.x(), pointsSpan.y());
                poseMask = Eigen::Vector4f(1, 1, 0, 1);
                const float requiredDistance = maxSpan * focalLength / smallerDimension;
                auto newCamPose = Eigen::Vector3f(midPoint.x(), midPoint.y(), requiredDistance + 0.1f);
                updateCameraPosition(newCamPose);
                updateCameraOrientation(180.f, 0.f, 0.f);
                break;
            }
            case VIEW_YZ: {
                maxSpan = std::max(pointsSpan.y(), pointsSpan.z());
                poseMask = Eigen::Vector4f(0, 1, 1, 1);
                const float requiredDistance = maxSpan * focalLength / smallerDimension;
                auto newCamPose = Eigen::Vector3f(requiredDistance + 0.1f, midPoint.y(), midPoint.z());
                updateCameraPosition(newCamPose);
                updateCameraOrientation(-90.f, 0.f, 90.f);
                break;
            }
            case VIEW_ZX : {
                maxSpan = std::max(pointsSpan.x(), pointsSpan.z());
                poseMask = Eigen::Vector4f(1, 0, 1, 1);
                const float requiredDistance = maxSpan * focalLength / smallerDimension;
                auto newCamPose = Eigen::Vector3f(midPoint.x(), requiredDistance + 0.1f, midPoint.z());
                updateCameraPosition(newCamPose);
                updateCameraOrientation(-90.f, -90.f, 180.f);
                break;
            }
            case VIEW_XZ : {
                maxSpan = std::max(pointsSpan.x(), pointsSpan.z());
                poseMask = Eigen::Vector4f(1, 0, 1, 1);
                const float requiredDistance = maxSpan * focalLength / smallerDimension;
                auto newCamPose = Eigen::Vector3f(midPoint.x(), -(requiredDistance + 0.1f), midPoint.z());
                updateCameraPosition(newCamPose);
                updateCameraOrientation(-90.f, 0.f, 0.f);
                break;
            }
            case VIEW_YX: {
                maxSpan = std::max(pointsSpan.y(), pointsSpan.x());
                poseMask = Eigen::Vector4f(1, 1, 0, 1);
                const float requiredDistance = maxSpan * focalLength / smallerDimension;
                auto newCamPose = Eigen::Vector3f(midPoint.x(), midPoint.y(), -(requiredDistance + 0.1f));
                updateCameraPosition(newCamPose);
                updateCameraOrientation(0.f, 0.f, 90.f);
                break;
            }
            case VIEW_ZY : {
                maxSpan = std::max(pointsSpan.y(), pointsSpan.z());
                poseMask = Eigen::Vector4f(0, 1, 1, 1);
                const float requiredDistance = maxSpan * focalLength / smallerDimension;
                auto newCamPose = Eigen::Vector3f(-(requiredDistance + 0.1f), midPoint.y(), midPoint.z());
                updateCameraPosition(newCamPose);
                updateCameraOrientation(0.f, -90.f, 180.f);
                break;
            }
            default:
                throw std::runtime_error("Incorrect mode");
        }

        // Draw the grid on the image
        const int gridSpan = getGridSpan(maxSpan); // [cm]
        const auto gridOffset = Eigen::Vector4f(10.f, 10.f, 10.f, 1.f);
        const Eigen::Vector4i gridStart = (minPoint_W - gridOffset).cast<int>() * 100; // [cm]
        const Eigen::Vector4i gridEnd = (maxPoint_W + gridOffset).cast<int>() * 100; // [cm]

        // Draw grid by iterating over one index and keeping another index constant
        auto drawGrid = [&image, &gridStart, &gridEnd, &gridSpan, this](long iteratingIdx, long constantIdx) {
            assert(0 <= iteratingIdx && iteratingIdx <= 2);
            assert(0 <= constantIdx && constantIdx <= 2);

            for (int majorCoord = gridStart[iteratingIdx];
                 majorCoord <= gridEnd[iteratingIdx];
                 majorCoord += gridSpan) {
                auto from_W = Eigen::Vector4f(0, 0, 0, 1);
                auto to_W = Eigen::Vector4f(0, 0, 0, 1);
                from_W[iteratingIdx] = static_cast<float>(majorCoord) / 100; // [m]
                to_W[iteratingIdx] = static_cast<float>(majorCoord) / 100; // [m]
                from_W[constantIdx] = static_cast<float>(gridStart[constantIdx]) / 100; // [m]
                to_W[constantIdx] = static_cast<float>(gridEnd[constantIdx]) / 100; // [m]

                const auto from_I = projectPose(from_W);
                const auto to_I = projectPose(to_W);

                if (from_I == cv::Point2f() || to_I == cv::Point2f()) {
                    continue;
                }

                cv::line(image, from_I, to_I, cv::Scalar(128, 128, 128), 1, cv::LINE_AA);
            }
        };

        switch (gridPlane) {
            case PLANE_NONE: {
                break;
            }
            case PLANE_XY: {
                drawGrid(0, 1);
                drawGrid(1, 0);
                break;
            }
            case PLANE_YZ: {
                drawGrid(1, 2);
                drawGrid(2, 1);
                break;
            }
            case PLANE_ZX: {
                drawGrid(0, 2);
                drawGrid(2, 0);
                break;
            }
            default:
                throw std::runtime_error("Incorrect grid plane mode");
        }

        // Draw the World frame
        // W - world
        // C - camera
        // I - image (2D)
        // CW - transformation from world to camera
        auto origin_W = Eigen::Vector4f(0.0, 0.0, 0.0, 1.0);
        auto xAxisEnd_W = Eigen::Vector4f(frameSize, 0.0, 0.0, 1.0);
        auto yAxisEnd_W = Eigen::Vector4f(0.0, frameSize, 0.0, 1.0);
        auto zAxisEnd_W = Eigen::Vector4f(0.0, 0.0, frameSize, 1.0);

        auto origin_I = projectPose(origin_W, poseMask);
        auto xAxisEnd_I = projectPose(xAxisEnd_W, poseMask);
        auto yAxisEnd_I = projectPose(yAxisEnd_W, poseMask);
        auto zAxisEnd_I = projectPose(zAxisEnd_W, poseMask);
        if (origin_I != cv::Point2f()) {
            const auto axisLabelOffset = cv::Point2f(10, 10);

            if (xAxisEnd_I != cv::Point2f()) {
                cv::arrowedLine(image, origin_I, xAxisEnd_I, cv::Scalar(0, 0, 255), lineThickness, cv::LINE_AA, 0,
                                0.05);
                cv::putText(image, "x", xAxisEnd_I + axisLabelOffset, cv::FONT_HERSHEY_COMPLEX, 0.5,
                            cv::Scalar(0, 0, 255), 1);
            }

            if (yAxisEnd_I != cv::Point2f()) {
                cv::arrowedLine(image, origin_I, yAxisEnd_I, cv::Scalar(0, 255, 0), lineThickness, cv::LINE_AA, 0,
                                0.05);
                cv::putText(image, "y", yAxisEnd_I + axisLabelOffset, cv::FONT_HERSHEY_COMPLEX, 0.5,
                            cv::Scalar(0, 255, 0), 1);
            }

            if (zAxisEnd_I != cv::Point2f()) {
                cv::arrowedLine(image, origin_I, zAxisEnd_I, cv::Scalar(255, 0, 0), lineThickness, cv::LINE_AA, 0,
                                0.05);
                cv::putText(image, "z", zAxisEnd_I + axisLabelOffset, cv::FONT_HERSHEY_COMPLEX, 0.5,
                            cv::Scalar(255, 0, 0), 1);
            }
        }

        // Draw the path history
        for (size_t i = 0; i + 1 < path.size(); ++i) {
            auto from = projectPose(path.at(i), poseMask);
            auto to = projectPose(path.at(i + 1), poseMask);

            cv::Point2f diff = to - from;
            const static constexpr float minDist = 1e-7f;
            if (diff.dot(diff) < minDist || from == cv::Point2f() || to == cv::Point2f()) {
                // short segment || invalid from || invalid to
                continue;
            }


            auto relativeHeight = [&i, this](long coordinate) {
                assert(0 <= coordinate && coordinate <= 2);

                return (path.at(i)[coordinate] - minPoint_W[coordinate] + path.at(i + 1)[coordinate] -
                        minPoint_W[coordinate]) * 0.5f /
                       (maxPoint_W[coordinate] - minPoint_W[coordinate]);
            };

            float relHeight;
            switch (viewMode) {
                case CUSTOM:
                case VIEW_XY:
                case VIEW_XZ: {
                    relHeight = relativeHeight(2);
                    break;
                }
                case VIEW_YX:
                case VIEW_YZ: {
                    relHeight = relativeHeight(0);
                    break;
                }
                case VIEW_ZX:
                case VIEW_ZY: {
                    relHeight = relativeHeight(1);
                    break;
                }
                default:
                    throw std::runtime_error("Incorrect mode");
            }

            const auto color1 = cv::Scalar(255, 255, 0);
            const auto color2 = cv::Scalar(0, 255, 255);
            cv::line(image, from, to, relHeight * color1 + (1.0f - relHeight) * color2, lineThickness, cv::LINE_AA);
        }

        auto vec3toVec4 = [](const Eigen::Vector3f& vec) {
            return Eigen::Vector4f(vec.x(), vec.y(), vec.z(), 1.0);
        };

        // Draw robot coordinate frame
        if (not lastPose.getPosition().isZero()) {
            const Eigen::Matrix3f rotMat = lastPose.getRotationMatrix();
            const auto eX = rotMat.col(0) * frameSize / 3;
            const auto eY = rotMat.col(1) * frameSize / 3;
            const auto eZ = rotMat.col(2) * frameSize / 3;

            auto from = projectPose(vec3toVec4(lastPose.getPosition()), poseMask);
            auto xTo = projectPose(vec3toVec4(lastPose.getPosition()) + Eigen::Vector4f(eX.x(), eX.y(), eX.z(), 0.0),
                                   poseMask);
            auto yTo = projectPose(vec3toVec4(lastPose.getPosition()) + Eigen::Vector4f(eY.x(), eY.y(), eY.z(), 0.0),
                                   poseMask);
            auto zTo = projectPose(vec3toVec4(lastPose.getPosition()) + Eigen::Vector4f(eZ.x(), eZ.y(), eZ.z(), 0.0),
                                   poseMask);


            cv::arrowedLine(image, from, xTo, cv::Scalar(0, 0, 255), lineThickness, cv::LINE_AA, 0, 0.05);
            cv::arrowedLine(image, from, yTo, cv::Scalar(0, 255, 0), lineThickness, cv::LINE_AA, 0, 0.05);
            cv::arrowedLine(image, from, zTo, cv::Scalar(255, 0, 0), lineThickness, cv::LINE_AA, 0, 0.05);

            // Format the timestamp to a nice string
            auto tsToString = [](const int64_t ts) {
                const int64_t s = ts / 1000000;
                const int64_t ms = ts % 1000000;
                const int64_t roundedMs = ms / 1000;

                // Determine whether the timestamp is in UTC or counting from 0 assuming nobody records longer than 1w
                if (s > 3600 * 24 * 7) {
                    auto newtime = std::chrono::system_clock::time_point(std::chrono::microseconds(ts));

                    std::string format = "UTC %F %T";
                    std::time_t tt = std::chrono::system_clock::to_time_t(newtime);
                    std::tm tm = *std::gmtime(&tt); //GMT (UTC)
                    //std::tm tm = *std::localtime(&tt); //Locale time-zone, usually UTC by default.
                    std::stringstream ss;
                    ss << std::put_time(&tm, format.c_str()) << "." << roundedMs;
                    return ss.str();
                } else {
                    // Simple timestamp in seconds . milliseconds
                    std::stringstream ss;
                    ss << "timestamp = " << s << "." << roundedMs << "s";
                    return ss.str();
                }
            };

            // Round to two decimal places
            auto roundTwo = [](float val) {
                const int value = static_cast<int>(lround(val * 100.f));
                return static_cast<float>(value) / 100;
            };

            // Text on the top of the frame:
            std::stringstream posText;
            posText << "position = [" << roundTwo(lastPose.getPosition().x()) << ", "
                    << roundTwo(lastPose.getPosition().y()) << ", "
                    << roundTwo(lastPose.getPosition().z())
                    << "]";
            cv::putText(image, posText.str(), cv::Point(15, 15),
                        cv::FONT_HERSHEY_COMPLEX, 0.5, cv::Scalar(255, 255, 255), 1);

            std::stringstream timeText;
            timeText << tsToString(lastTimestamp);
            cv::putText(image, timeText.str(), cv::Point(15, 35), cv::FONT_HERSHEY_COMPLEX, 0.5,
                        cv::Scalar(255, 255, 255),
                        1);

            if (gridPlane != GridPlane::PLANE_NONE) {
                std::stringstream gridText;
                gridText << "grid span = " << gridSpan << "cm";
                cv::putText(image, gridText.str(), cv::Point(15, 55), cv::FONT_HERSHEY_COMPLEX, 0.5,
                            cv::Scalar(255, 255, 255),
                            1);
            }
        }

        return image;
    }

    /**
     * Reset the pose history and set an offset to the last pose.
     */
    void reset() {
        if (path.empty()) {
            return;
        }
        assert(!timestamps.empty());
        lastTimestamp = timestamps.back();

        path.clear();
        timestamps.clear();
        const auto T_WO1 = T_WO;
        const auto T_O1O2 = lastPose;
        const auto T_WO2 = T_WO1 * T_O1O2;
        T_WO = T_WO2;
        initMinMax();
    }


private:

    /**
     * Convert a pose from 3D coordinates to image frame.
     *
     * @param pose_W pose to project in the World frame
     * @param flattenZ if true, Z coordinate will be set to zero
     * @return
     */
    [[nodiscard]] cv::Point2f
    projectPose(const Eigen::Vector4f& pose_W,
                const Eigen::Vector4f& mask = Eigen::Vector4f(1.f, 1.f, 1.f, 1.f)) const {
        const auto maskedPose_W = pose_W.cwiseProduct(mask);
        const auto pose_C = T_CW * maskedPose_W;
        if (pose_C.z() < 0) {
            // Point is behind the camera
            return cv::Point2f();
        }

        const auto pose_I = camMat * pose_C.block<3, 1>(0, 0) / pose_C.z();
        return cv::Point2f(pose_I.x(), pose_I.y());
    }

    /**
     * Update the camera matrix based on the current image size.
     */
    void refreshCameraMatrix() {
        camMat << focalLength, 0, static_cast<float>(imageSize.width) / 2.f,
                0, focalLength, static_cast<float>(imageSize.height) / 2.f,
                0, 0, 1;
    }

    /**
     * Initialize minimum and maximum point coordinates.
     */
    void initMinMax() {
        minPoint_W = Eigen::Vector4f(-0.5, -0.5, -0.5, 1.0);
        maxPoint_W = Eigen::Vector4f(0.5, 0.5, 0.5, 1.0);
    }

private:

    // Image on which the visualization is drawn
    cv::Size2i imageSize;
    int lineThickness = 1; // [px]
    GridPlane gridPlane = GridPlane::PLANE_XY;

    // Min and max coordinates that can be displayed on the image in meters
    Eigen::Vector4f minPoint_W;
    Eigen::Vector4f maxPoint_W;

    // Size of a robot coordinate frame visualization
    float frameSize = 1.0; // [m]

    // Motion path of the robot
    std::vector<Eigen::Vector4f> path;
    std::vector<int64_t> timestamps;

    // For convenience keep the last pose separately
    kindr::minimal::QuatTransformationTemplate<float> lastPose;
    int64_t lastTimestamp;

    // Camera details
    Eigen::Vector3f cameraPosition;
    Eigen::Quaternionf cameraOrientation;
    const float focalLength = 100;
    Eigen::Matrix<float, 3, 3> camMat;
    Eigen::Matrix<float, 4, 4> T_CW, T_WC;
    Mode viewMode = Mode::CUSTOM;

    // Offset of the positions
    kindr::minimal::QuatTransformationTemplate<float> T_WO; // Transformation from "World" to "Offset" frame


};
