//
// Created by rokas on 2020-10-07.
//

#pragma once

#include <cstdint>

class ExecutionTimer {
private:
    int64_t lastExecutionTime = 0LL;

    // 1Hz
    int64_t dt = 1000000LL;

public:
    ExecutionTimer(int64_t timestampNow, uint32_t frequency);

    ExecutionTimer();

    bool timeToRun(int64_t timestampNow);

    void reset();
};

