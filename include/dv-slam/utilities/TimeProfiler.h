#pragma once

#include <ctime>
#include <cstdlib>
#include <chrono>

#include <iostream>
#include <iomanip>

namespace Utils {
    class TimeProfiler {
    public:
        TimeProfiler() {
            tic();
        }

        void tic() {
            start = std::chrono::system_clock::now();
        }

        double toc() {
            end = std::chrono::system_clock::now();
            std::chrono::duration<double> elapsed_seconds = end - start;
            last = elapsed_seconds.count();
            if (last < minimum) {
                minimum = last;
            }
            if (last > maximum) {
                maximum = last;
            }
            sum += last;
            counter++;
            average = sum / static_cast<double>(counter);
            return last * 1000; // returns millisec
        }

        void toc_print(const std::string &name) {
            std::cout << std::fixed << "Done [" << name << "]: " << toc() << " ms" << std::endl;
        }

        friend std::ostream &operator<<(std::ostream &os, const TimeProfiler &profiler) {
            os << std::fixed << std::setprecision(3)  << "min[" << (profiler.minimum * 1000.) << "]; max["
               << (profiler.maximum * 1000.) << "]; avg[" << (profiler.average * 1000.) << "]; last[" << (profiler.last * 1000.) << "]";
            return os;
        }

    private:
        std::chrono::time_point<std::chrono::system_clock> start, end;
        double minimum = std::numeric_limits<double>::max();
        double maximum = std::numeric_limits<double>::min();
        double sum = 0.0;
        double average = 0.0;
        double last = 0.0;
        uint64_t counter = 0ULL;
    };
}
