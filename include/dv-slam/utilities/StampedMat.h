//
// Created by rokas on 2021-01-21.
//

#pragma once

#include <opencv2/core/mat.hpp>

namespace slam {
    class StampedMat {
    public:
        int64_t timestamp;
        int64_t startTimestamp = -1LL;
        int64_t endTimestamp = -1LL;
        cv::Mat frame;

        StampedMat(int64_t timestamp, int64_t startTimestamp, int64_t endTimestamp, cv::Mat frame);

        StampedMat(int64_t timestamp, cv::Mat frame);
    };
}

