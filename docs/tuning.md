# Parameter tuning for SLAM

This tutorial gives an additional information on available tunable parameters of SLAM module and how they can be tuned 
to get better performance in terms of accuracy or execution speed.

## Available parameters

Preview of available parameters in the DV GUI:
![Parameter preview](images/params.png)

Description of individual parameters:

* Calibration: select the xml file from calibration procedure or the yaml calibration file from ROS.
* Event half slice: Time interval in microseconds that is used to select events for sparse depth map creation, 
increasing this value would use more events to construct sparse point cloud.
* Use event block matcher: Select this checkbox to use original block matcher by the original authors of the algorithm.
If this is not checked, an SGBM stereo block matcher is used that can be accelerated using a GPU (if it was enabled 
during) compilation.
* Debug mode: If selected, the module will produce additional visualization that can slow down operation of the module.
Disabling debug mode will reduce CPU load.
* Depth only: Depth only mode is useful for testing sparse depth map, it disables tracking and only performs
sparse depth estimation. it can be used test whether there are any issues with depth estimation (that can be related 
to camera synchronization or calibration).
* Mapping frequency: Choose the frequency of mapping execution, anything between 15-25 Hz is good enough, less can be 
used on less powerful systems. 
* Max depth: maximum distance from camera that is estimated. Points that are estimated with larger distance are discarded.
* Min depth: minimum distance from camera that is estimated. Points that are estimated with lower distance are discarded.
* Min depth points to initialize: minimum number of depth points that has to be successfully estimated for the algorithm
to initialize.
* Minimum events to estimate: Minimum number of events during a frame window for tracking to execute. Some small value
* Sparse depth point count: Amount of points that is aggregated in the point cloud used for tracking. The exact amount
of points can be larger so a small extent.
* Stdvar threshold: This value allows the control of depth point sampling, by reducing the value, resulting depth point
cloud is more sparse, but selected points are more reliable. 
* Tracker contrast: Increase this coefficient to increase brightness of the image used in tracking. It can improve 
tracking accuracy if edges in the accumulated frame is not bright enough. 

## Motion compensation 

This project also includes a wrapped module for motion compensated events. The algorithm used for motion compensation
is [better flow](https://github.com/better-flow/better-flow). To use motion compensation replace the accumulators 
with motion compensation modules. An example usage is provided in project 
`<project_root>/runtime-templates/motion-compensated-slam.xml`. 
 
> NOTICE: Motion compensated images has lower brightness compared to accumulated images, it is a good idea to increase
> `Tracker contrast` value to between 2.0 or 3.0, it improves the tracking accuracy when used with motion compensation.

Motion compensation can increase tracking accuracy when the camera can move with high motion and it generates motion 
blur in the accumulated images. Motion compensation roughly estimates the motion and compensates events accordingly, 
reducing the amount of motion blur.
