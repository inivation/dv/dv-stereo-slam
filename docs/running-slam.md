# Running the SLAM algorithm

## Building the project

Before running, make sure you have successfully built the module (following the steps in [build](build.md) tutorial).

## Preparing the environment

DV GUI application has to load the built module in order to execute, please add the build directory to module search path.
Path can be added by clicking `Add module` -> `Modify module search path` and adding selecting the path to search list:

![Module search path](images/run-1.png)

## Calibration

If you haven't done calibration yet, follow the stereo [calibration](calibration.md) tutorial. Please take note of
calibration file, that has to be provided for the module later on.

ROS calibrations are also supported, please see `config/calibs` directory for `.yaml` files that can serve as an 
example how to provide the calibration. The module tries to identify the type of calibration by the extension, so 
module will try to parse yaml `left.yaml` and `right.yaml` if a file with yaml extension is selected. 

## Running module with stereo camera

To run the module, please run the DV GUI software and follow these steps:

1. Click `File` -> `Open Project` in the menu and select stereo slam project file from 
`<project_root>/runtime-templates/stereo-slam.xml`. You should see the project structure:
   ![The project structure](images/run-2.png)

2. Choose left and right camera sensors accordingly by their ID (ID can be found on the back side of the camera):
   ![Selecting sensors](images/run-3.png)

3. Choose stereo calibration file in the SLAM module menu:
   ![Selecting calibration](images/run-4.png)

4. During first run, launch the camera inputs and click `Reset Timestamps` to synchronise the cameras:
   ![Reset timestamps](images/run-5.png)

5. Start the SLAM module, move the camera, you should be able to see a depth map and depth points following the 
edges in the `Output` tab.

 ## Tuning the performance
 
If you can see the correct visualization in the output menu, you can also read the additional [tuning guide](tuning.md) 
that can help to achieve better performance.
