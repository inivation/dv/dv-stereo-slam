# Running sample datasets

## Available datasets

The public datasets are available for download following [this link](https://release.inivation.com/?prefix=datasets/dv-stereo-slam/).

Some datasets (rpg, upenn) are just repackaged data from the original [project site of ESVO](https://sites.google.com/view/esvo-project-page/home). The data
is provided in aedat4 format for DV runtime instead of rosbag format.

Datasets contain camera calibration, recorded event data and ground truth if available.

## Running a dataset

Different datasets were captured with different cameras, so reproducing the results requires different
parameters. Preconfigured project files for different datasets are prepared with the correct parameters 
values for the dataset in `runtime-templates` directory:

* RPG datasets: `rpg-dataset.xml`
* UPENN datasets: `upenn-dataset.xml`
* CAR datasets (by iniVation): `car-slam.xml`

To run a dataset, please download and extract data from the link in top of the document and follow these steps:

1. Build the project as described in the [build tutorial](build.md), make sure the project compiles and installs succesfully.
2. Start the DV GUI, open project XML file from `runtime-templates` directory, projects are named accordingly to
   dataset names.
3. Click on the `Input_file` module and choose input file path (aedat4 from the extracted archive).
4. Click on the `Slam_esvomapping` module and choose calibration file from the extracted archive (if 
   calibration directory contains 2 yaml files, you can select either one).
   - (Car dataset only) Click on the `Slam_event mask` module and select mask image which is provided 
      in the calibration directory. 
5. Run all modules, you should see the estimated trajectory and preview images in the `Output` tab. 


## Evaluating against ground truth trajectory

To evaluate the estimation error of the trajectory, a very fine tool [rpg_trajectory_evaluation](https://github.com/uzh-rpg/rpg_trajectory_evaluation) can be used.
The module supports trajectory output that is compatible with the mentioned tool.

To perform accuracy evaluation follow these steps:

1. Clone and install [rpg_trajectory_evaluation](https://github.com/uzh-rpg/rpg_trajectory_evaluation) tool, 
   follow installation instruction in the repository.
2. Create a directory of your choice to store the trajectory output files.
3. Copy ground truth file `stamped_groundtruth.txt` file from the downloaded archive into the directory.
4. In `dv-gui`, set the `Global map output` parameter to the same directory containing the ground truth file.
5. Run the SLAM pipeline, wait for it to finish, the estimated trajectory will be saved there.
6. Run the evaluation tool:
```
cd rpg_trajectory_evaluation/scripts
python analyze_trajectory_single.py --png --recalculate_errors /path/to/selected/output/directory
```

The tool will calculate various statistical metrics for the error, draw trajectory previous. You will find
all of that under the same output directory.
