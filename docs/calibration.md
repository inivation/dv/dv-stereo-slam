# Stereo camera calibration

## Calibration in DV

ESVO SLAM algorithm is dependent on a good calibration to perform accurate camera pose tracking and reconstruction (as is any other geometry based SLAM algorithm).
DV software provides modules and procedures to perform stereo calibration. 
A detailed tutorial is available in the official documentation [here](https://inivation.gitlab.io/dv/dv-docs/docs/tutorial-calibration/).

This is a comprehensive tutorial how to perform calibration on the stereo camera.

## Monocular calibration on independent sensors

To achieve best results, individual sensor intrinsic parameters should be calibrated first. Follow these steps to perform the monocular calibration:

1. Please download [the PDF](https://inivation.gitlab.io/dv/dv-docs/docs/assets/calib.io_circlesA_279x210_9x12_28.2842_15.pdf) with the pattern correct calibration pattern. Print it, measure the circle size after printing to make sure the scaling is correct and attach it to a rigid flat surface (like a sheet of cardboard).

2. Connect DV cameras to your computer. 

3. Start DV GUI software, select `File` > `Open Project`, choose preconfigured stereo calibration project that is included in this project: `<project_root>/runtime-templates/stereo-calibration.xml`.

4. Choose calibration output directory `Output Calibration Directory` here:
![Output directory selection](images/calib-1.png)

5. In the configuration panel choose a camera to read input images from:
![Input device selection](images/calib-2.png)

6. In the `Output` tab you should see images from the camera, show the calibration to the camera. The detected patterns will be highlighted in the display. A collected sample pattern location will be highlighted by green overlay, try to collect samples at different scales and rotations so that the image would be uniformly overlaid by the green color.
![Sample display of the detected pattern](images/calib-3.png)

8. With enough patterns have been collected, review collected samples using `Keep` and `Discard` buttons. Discard low quality, blurred, or incorrectly detected samples.

9. The calibration will be performed when enough samples will be selected. Please note the reprojection error that will be printed in the `Runtime Log`, an error value less than 0.2 is acceptable, less than 0.1 can be considered good.

10. Calibration file will be saved in the output directory, please repeat steps 5-9 for the second camera.

## Stereo calibration

At this point both camera should be calibrated independently and achieved at least acceptable reprojection error values.
To perform a stereo calibration, please follow these steps:

1. Connect both cameras to the computer. Choose correct camera input sources in the image capture settings (INPUT1 must be connected to the left sensor, INPUT2 to the right sensor, camera IDs can be found on back side of the camera):
![Selecting stereo sensors](images/calib-4.png)

> ! NOTICE: Make sure that both image inputs are connected as in the image above.

2. Initiate stereo camera timestamp synchronization by clicking `Reset timestamps` button here (it is required to reset the timestamps after restarting any cameras or DV GUI software):
![Reset timestamps button](images/calib-5.png)

3. Choose monocular calibration files that achieved best results from monocular calibration.
![Reset timestamps button](images/calib-6.png)

4. Collect samples in the same manner as described in the steps 6 and 7 of monocular calibration tutorial.

5. With enough reviewed samples collected, calibration is performed. You can find calibration reprojection error and epipolar error value in the `Runtime Log`, a good calibration should have reprojection error < 0.2 and epipolar error < 3.5, but lower values are better. 


